<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
  <script async="" src="https://query.yahooapis.com/v1/public/yql?format=json&amp;rnd=20192620&amp;diagnostics=true&amp;callback=jQuery1124013337594877261894_1553348468389&amp;q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text=%22Seattle%22)%20and%20u=%22c%22&amp;_=1553348468390"></script>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<title>{{$title}}</title>
   <meta name="keywords" content="{{$meta_keyword}}">
<meta name="description" content="{{$meta_description}}">
<!-- Social Warfare v3.3.3 https://warfareplugins.com --><style>@font-face {font-family: "sw-icon-font";src:url("fonts/sw-icon-font.eot");src:url("fonts/sw-icon-font.eot#iefix") format("embedded-opentype"),url("fonts/sw-icon-font.woff") format("woff"),
  url("fonts/sw-icon-font.ttf") format("truetype"),url("fonts/sw-icon-font.svg#1445203416") format("svg");font-weight: normal;font-style: normal;}</style>
<!-- Social Warfare v3.3.3 https://warfareplugins.com -->


<!-- This site is optimized with the Yoast SEO plugin v9.2.1 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="http://seniornews.com/">
<meta property="og:locale" content="en_US">
<meta property="og:type" content="website">
<meta property="og:title" content="Home - SeniorNews">
<meta property="og:url" content="http://seniornews.com/">
<meta property="og:site_name" content="SeniorNews">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="Home - SeniorNews">
<meta name="twitter:image" content="http://seniornews.com/wp-content/uploads/2019/01/home-with-lawn-270x205.jpg">
<script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"WebSite","@id":"#website","url":"http:\/\/seniornews.com\/","name":"SeniorNews","potentialAction":{"@type":"SearchAction","target":"http:\/\/seniornews.com\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"Organization","url":"http:\/\/seniornews.com\/","sameAs":[],"@id":"http:\/\/seniornews.com\/#organization","name":"SeniorNews.com","logo":"http:\/\/seniornews.com\/wp-content\/uploads\/2018\/10\/logo.png"}</script>
<!-- / Yoast SEO plugin. -->

<link rel="dns-prefetch" href="//fonts.googleapis.com">
<link rel="dns-prefetch" href="//s.w.org">
<link rel="alternate" type="application/rss+xml" title="SeniorNews » Feed" href="http://seniornews.com/feed/">
<link rel="alternate" type="application/rss+xml" title="SeniorNews » Comments Feed" href="http://seniornews.com/comments/feed/">
    <script type="text/javascript">
      window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/seniornews.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=1cf89da66f4d2b1e20f2e4dd9577c7bc"}};
      !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
    </script><script src="http://seniornews.com/wp-includes/js/wp-emoji-release.min.js?ver=1cf89da66f4d2b1e20f2e4dd9577c7bc" type="text/javascript" defer=""></script>
    <!-- managing ads with Advanced Ads – https://wpadvancedads.com/ --><script>
          advanced_ads_ready=function(){var fns=[],listener,doc=typeof document==="object"&&document,hack=doc&&doc.documentElement.doScroll,domContentLoaded="DOMContentLoaded",loaded=doc&&(hack?/^loaded|^c/:/^loaded|^i|^c/).test(doc.readyState);if(!loaded&&doc){listener=function(){doc.removeEventListener(domContentLoaded,listener);window.removeEventListener("load",listener);loaded=1;while(listener=fns.shift())listener()};doc.addEventListener(domContentLoaded,listener);window.addEventListener("load",listener)}return function(fn){loaded?setTimeout(fn,0):fns.push(fn)}}();
      </script><style type="text/css">
img.wp-smiley,
img.emoji {
  display: inline !important;
  border: none !important;
  box-shadow: none !important;
  height: 1em !important;
  width: 1em !important;
  margin: 0 .07em !important;
  vertical-align: -0.1em !important;
  background: none !important;
  padding: 0 !important;
}
</style>
<link rel="stylesheet" id="contact-form-7-css" href="{{asset('public/assets/senior_theme/css/styles.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="dashicons-css" href="{{asset('public/assets/senior_theme/css/dashicons.min.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="wpmm_fontawesome_css-css" href="{{asset('public/assets/senior_theme/css/font-awesome.min.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="wpmm_css-css" href="{{asset('public/assets/senior_theme/css/wpmm.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="featuresbox_css-css" href="{{asset('public/assets/senior_theme/css/wpmm-featuresbox.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="postgrid_css-css" href="{{asset('public/assets/senior_theme/css/wpmm-gridpost.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="font-awesome-css" href="{{asset('public/assets/senior_theme/css/font-awesome.min_1.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="social_warfare-css" href="{{asset('public/assets/senior_theme/css/style.min.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="bootstrap-css" href="{{asset('public/assets/senior_theme/css/bootstrap.min.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="mean-menu-css" href="{{asset('public/assets/senior_theme/css/meanmenu.min.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="hashnews-default-style-css" href="{{asset('public/assets/senior_theme/css/theme-default.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="hashnews-theme-main-css" href="{{asset('public/assets/senior_theme/css/theme-main.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="parent-style-css" href="{{asset('public/assets/senior_theme/css/style_1.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="hashnews-font-css" href="{{asset('public/assets/senior_theme/css/css.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="magnific-popup-css" href="{{asset('public/assets/senior_theme/css/magnific-popup.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="animate-css" href="{{asset('public/assets/senior_theme/css/animate_1.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="slick-css" href="{{asset('public/assets/senior_theme/css/slick.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="owl-carousels-css" href="{{asset('public/assets/senior_theme/css/owl.carousel.min.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="rypp-css" href="{{asset('public/assets/senior_theme/css/rypp.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="hashnews-style-css" href="{{asset('public/assets/senior_theme/css/style.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="hashnews-blog-style-css" href="{{asset('public/assets/senior_theme/css/blog-post.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="hashnews-main-style-css" href="{{asset('public/assets/senior_theme/css/theme-style.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="hashnews-responsive-css" href="{{asset('public/assets/senior_theme/css/responsive.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="hashnews-dynamic-style-css" href="{{asset('public/assets/senior_theme/css/dynamic-style.css')}}" type="text/css" media="all">
<style id="hashnews-dynamic-style-inline-css" type="text/css">

      .single-blog .blog-wrap.fall-prevention-tips .meta .meta-item.category,.post .post-wrap .category.fall-prevention-tips{
          background-color:00c8fa;
        }.single-blog .blog-wrap.fall-prevention-tips .title a:hover,.single-blog .blog-wrap.fall-prevention-tips .meta a.meta-item.author:hover,.single-blog .blog-wrap.fall-prevention-tips .content .read-more:hover,.single-blog .blog-wrap.fall-prevention-tips .meta .meta-item i,.post.format-link .single-blog .blog-wrap.fall-prevention-tips .post-title::before{
          color:00c8fa;
        }.single-blog .blog-wrap.safety-fall-prevention .meta .meta-item.category,.post .post-wrap .category.safety-fall-prevention{
          background-color:00c8fa;
        }.single-blog .blog-wrap.safety-fall-prevention .title a:hover,.single-blog .blog-wrap.safety-fall-prevention .meta a.meta-item.author:hover,.single-blog .blog-wrap.safety-fall-prevention .content .read-more:hover,.single-blog .blog-wrap.safety-fall-prevention .meta .meta-item i,.post.format-link .single-blog .blog-wrap.safety-fall-prevention .post-title::before{
          color:00c8fa;
        }.single-blog .blog-wrap.travel .meta .meta-item.category,.post .post-wrap .category.travel{
          background-color:00c8fa;
        }.single-blog .blog-wrap.travel .title a:hover,.single-blog .blog-wrap.travel .meta a.meta-item.author:hover,.single-blog .blog-wrap.travel .content .read-more:hover,.single-blog .blog-wrap.travel .meta .meta-item i,.post.format-link .single-blog .blog-wrap.travel .post-title::before{
          color:00c8fa;
        }
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    

     
     
     
     
    @media (max-width: 767px) { 
       
       
       
      
    }
    
    
    
    
    
    section .page-banner{
        background-image: url( images/hoodie_7_back.jpg); 
      }

    
    

    
    
    

    
    
    

    
    
    
    
    
    

    
    
    
    

    
    
    
    
</style>
<link rel="stylesheet" id="kc-general-css" href="{{asset('public/assets/senior_theme/css/kingcomposer.min.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="kc-animate-css" href="{{asset('public/assets/senior_theme/css/animate.css')}}" type="text/css" media="all">
<link rel="stylesheet" id="kc-icon-1-css" href="{{asset('public/assets/senior_theme/css/icons.css')}}" type="text/css" media="all">
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/jquery-migrate.min.js')}}"></script>
<script type="text/javascript">
/* <![CDATA[ */
var ajax_tptn_tracker = {"ajax_url":"http:\/\/seniornews.com\/","top_ten_id":"64","top_ten_blog_id":"1","activate_counter":"11","tptn_rnd":"924015014"};
/* ]]> */
</script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/top-10-tracker.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/wpmm-featuresbox.js')}}"></script>
<script type="text/javascript">
/* <![CDATA[ */
var postgrid_ajax_load = {"ajax_url":"http:\/\/seniornews.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/wpmm-gridpost.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/advanced.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/cfp.min.js')}}"></script>
<script type="text/javascript">
/* <![CDATA[ */
var advanced_ads_pro_ajax_object = {"ajax_url":"http:\/\/seniornews.com\/wp-admin\/admin-ajax.php","lazy_load_module_enabled":"","lazy_load":{"default_offset":0,"offsets":[]}};
/* ]]> */
</script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/base.min.js')}}"></script>
<link rel="https://api.w.org/" href="http://seniornews.com/wp-json/">
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://seniornews.com/xmlrpc.php?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://seniornews.com/wp-includes/wlwmanifest.xml"> 
<link rel="shortlink" href="http://seniornews.com/">
<link rel="alternate" type="application/json+oembed" href="http://seniornews.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fseniornews.com%2F">
<link rel="alternate" type="text/xml+oembed" href="http://seniornews.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fseniornews.com%2F&amp;format=xml">
<script type="text/javascript">var kc_script_data={ajax_url:"http://seniornews.com/wp-admin/admin-ajax.php"}</script><style type="text/css">0</style><script type="text/javascript">
(function(url){
  if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
  var addEvent = function(evt, handler) {
    if (window.addEventListener) {
      document.addEventListener(evt, handler, false);
    } else if (window.attachEvent) {
      document.attachEvent('on' + evt, handler);
    }
  };
  var removeEvent = function(evt, handler) {
    if (window.removeEventListener) {
      document.removeEventListener(evt, handler, false);
    } else if (window.detachEvent) {
      document.detachEvent('on' + evt, handler);
    }
  };
  var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
  var logHuman = function() {
    if (window.wfLogHumanRan) { return; }
    window.wfLogHumanRan = true;
    var wfscr = document.createElement('script');
    wfscr.type = 'text/javascript';
    wfscr.async = true;
    wfscr.src = url + '&r=' + Math.random();
    (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
    for (var i = 0; i < evts.length; i++) {
      removeEvent(evts[i], logHuman);
    }
  };
  for (var i = 0; i < evts.length; i++) {
    addEvent(evts[i], logHuman);
  }
})('//seniornews.com/?wordfence_lh=1&hid=64F2B7403AFD0040221E57DA26242B58');
</script><style type="text/css"></style><style type="text/css"></style><script type="text/javascript">
    ;var advadsCfpQueue = [], advadsCfpExpHours = 3;
    var advadsCfpClickLimit = 3;
    ;
    var advadsCfpPath = '';
    var advadsCfpDomain = '';
    var advadsCfpAd = function( adID ){
      if ( 'undefined' == typeof advadsProCfp ) { advadsCfpQueue.push( adID ) } else { advadsProCfp.addElement( adID ) }
    };
    </script> 
  <link rel="shortcut icon" href="favicon.ico">
      <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" href="favicon.ico">
      <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon" sizes="114x114" href="favicon.ico">
      <!-- For iPad -->
    <link rel="apple-touch-icon" sizes="72x72" href="favicon.ico">
      <!-- For iPad Retina display -->
    <link rel="apple-touch-icon" sizes="144x144" href="favicon.ico">
    <script async="" src="{{asset('public/assets/senior_theme/js_1')}}"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '');
  </script>

    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
    <style type="text/css" title="dynamic-css" class="options-output">.page-wrapper{padding-top:0;padding-bottom:0;}</style><script type="text/javascript"></script><style type="text/css" id="kc-css-general">.kc-off-notice{display: inline-block !important;}.home_categories .body{
 height:343px;
 overflow-y:scroll;
}
.home .post .date{
display:none !important;
}
.home_pg_mid .image img{
min-height:443px !important;
}
div#tabpopularnews .widget-title{
 display:none;
}
div#tabpopularnews .tptn_after_thumb .tptn_link{
 font-weight:600;
 font-size:15px;
 line-height:24px;
 margin-bottom:0;
 color:#000;
 float:right;
width:53%;
}

div#tabpopularnews ul{
 padding:0;
}
div#tabpopularnews ul li{
 margin-bottom:20px;
 padding-bottom:20px;
 border-bottom:1px solid #f1f1f1;
}.kc-container{max-width:1170px;}</style><style type="text/css" id="kc-css-render">@media only screen and (min-width:1000px) and (max-width:5000px){body.kc-css-system .kc-css-566380{width:25%;}body.kc-css-system .kc-css-360630{width:50%;}body.kc-css-system .kc-css-64397{width:25%;}body.kc-css-system .kc-css-839854{width:66%;}body.kc-css-system .kc-css-820959{width:34%;}body.kc-css-system .kc-css-986219{width:33.33%;}body.kc-css-system .kc-css-819780{width:33.33%;}body.kc-css-system .kc-css-287270{width:33.33%;}body.kc-css-system .kc-css-550499{width:66.66%;}body.kc-css-system .kc-css-894158{width:33.33%;}body.kc-css-system .kc-css-90991{width:50%;}body.kc-css-system .kc-css-475886{width:50%;}}.kc-css-460748 .kc_column{padding-left:1px;padding-right:1px;}.kc-css-460748>.kc-wrap-columns{margin-left:-1px;margin-right:-1px;width:calc(100% + 2px);}body.kc-css-system .kc-css-89402{margin-bottom:50px;}body.kc-css-system .kc-css-882802{margin-bottom:50px;}body.kc-css-system .kc-css-154885 .post-block-wrapper .head .title{color:#008bff;}body.kc-css-system .kc-css-154885 .post-block-wrapper .head::before,body.kc-css-system .kc-css-154885 .post-block-wrapper .head::after{background-color:#008bff;}body.kc-css-system .kc-css-417795 .post-block-wrapper .head .title{color:#008bff;}body.kc-css-system .kc-css-417795 .post-block-wrapper .head::before,body.kc-css-system .kc-css-417795 .post-block-wrapper .head::after{background-color:#008bff;}body.kc-css-system .kc-css-841050 .sidebar-block-wrapper{margin-bottom:40px;}body.kc-css-system .kc-css-316626 .post-block-wrapper .head .title{color:#ff58c8;}body.kc-css-system .kc-css-316626 .post-block-wrapper .head::before,body.kc-css-system .kc-css-316626 .post-block-wrapper .head::after{background-color:#ff58c8;}body.kc-css-system .kc-css-316626 .post-block-carousel .slick-arrow:hover,body.kc-css-system .kc-css-316626 .post .post-wrap .content .read-more:hover,body.kc-css-system .kc-css-316626 .post-block-carousel .slick-arrow:hover{color:#ff58c8;}body.kc-css-system .kc-css-316626 .post-block-carousel.gadgets-post-carousel .slick-arrow:hover,body.kc-css-system .kc-css-316626 .post-block-carousel .slick-arrow:hover{border:1px solid #ff58c8;}body.kc-css-system .kc-css-696555 .post-block-carousel .slick-arrow:hover,body.kc-css-system .kc-css-696555 .post .post-wrap .content .read-more:hover,body.kc-css-system .kc-css-696555 .post-block-carousel .slick-arrow:hover{color:#00c8fa;}body.kc-css-system .kc-css-696555 .post-block-carousel.gadgets-post-carousel .slick-arrow:hover,body.kc-css-system .kc-css-696555 .post-block-carousel .slick-arrow:hover{border:1px solid #00c8fa;}@media only screen and (max-width:999px){body.kc-css-system .kc-css-839854{padding-left:15px;width:100%;}body.kc-css-system .kc-css-820959{margin-top:50px;width:100%;}body.kc-css-system .kc-css-986219{width:50%;}body.kc-css-system .kc-css-819780{width:50%;}body.kc-css-system .kc-css-287270{margin-top:50px;width:100%;}body.kc-css-system .kc-css-550499{width:100%;}body.kc-css-system .kc-css-894158{margin-top:50px;padding-left:0px;width:100%;}body.kc-css-system .kc-css-264031{width:50%;}}@media only screen and (max-width:767px){body.kc-css-system .kc-css-820959{padding-right:15px;}body.kc-css-system .kc-css-986219{width:100%;}body.kc-css-system .kc-css-819780{margin-top:50px;width:100%;}body.kc-css-system .kc-css-894158{padding-right:15px;padding-left:15px;}body.kc-css-system .kc-css-264031{width:100%;}body.kc-css-system .kc-css-316626 .post-block-wrapper{margin-top:50px;margin-bottom:50px;}}</style><script async="async" src="{{asset('public/assets/senior_theme/js/gpt.js')}}"></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/51534170/SeniorNews_Header', [728, 90], 'div-gpt-ad-1539377253505-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });

  googletag.cmd.push(function() {
    googletag.defineSlot('/51534170/SeniorNews_Sidebar', [300, 250], 'div-gpt-ad-1539378220331-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>

<meta name="p:domain_verify" content="0bd5ad3a0acecc898310d607b825ae19">
<link rel="preload" href="https://adservice.google.com.vn/adsid/integrator.js?domain=" as="script"><script type="text/javascript" src="https://adservice.google.com.vn/adsid/integrator.js?domain="></script><link rel="preload" href="https://adservice.google.com/adsid/integrator.js?domain=" as="script"><script type="text/javascript" src="https://adservice.google.com/adsid/integrator.js?domain="></script><script src="https://securepubads.g.doubleclick.net/gpt/pubads_impl_2019031802.js?21063409" async=""></script><script type="text/javascript" async="" src="//seniornews.com/?wordfence_lh=1&amp;hid=64F2B7403AFD0040221E57DA26242B58&amp;r=0.39491967030235786"></script></head>
   
   
  

<body class="home page-template page-template-page-full-width page-template-page-full-width-php page page-id-64 kingcomposer kc-css-system wp-megamenu group-blog wide-layout-active">


  <div id="page" class="site site-wrapper wide-layout">
    <div id="hashnews">
      <div class="header-top ">
  <div class="container">
    <div class="row">
      <div class="header-top-links col-md-9 col-6 left_content">
                  <!-- Header Links -->
            <ul class="header-links ">
              <li class="disabled block d-none d-md-block"><a href="#">
                <i class="fa fa-clock-o"></i> 
                {{Carbon\Carbon::now()->toFormattedDateString()}}</a>
              </li>
              
              <li>
                    <a target="_blank" href="{{url('/login')}}"><i class="fa fa-sign-in"></i>My Account</a> 
                  </li>               
              <li>
                              </li>
            </ul>
                </div> 
      <div class="header-top-social col-md-3 col-6">
                  <div class="header-top-social">
              <!-- Header Social -->
              <div class="header-social ">
                <a class="facebook social-icon" href="https://www.facebook.com/seniorcommunity" title="Facebook"><i class="fa fa-facebook"></i></a><a class="twitter social-icon" href="https://twitter.com/senioronline" title="Twitter"><i class="fa fa-twitter"></i></a><a class="instagram social-icon" href="https://www.instagram.com/senior_com/" title="Instagram"><i class="fa fa-instagram"></i></a><a class="pinterest social-icon" href="https://www.pinterest.com/seniorcom" title="Pinterest"><i class="fa fa-pinterest"></i></a><a class="linkedin social-icon" href="https://www.linkedin.com/company/senior.com" title="Linkedin"><i class="fa fa-linkedin"></i></a><a class="youtube social-icon" href="https://www.youtube.com/channel/UCSRS0tt6Pc8QolneZNgaykA?view_as=subscriber" title="Youtube"><i class="fa fa-youtube"></i></a>              </div>
            </div>
            
                </div>
    </div>
  </div>
</div>
<!-- Header Start -->
<div class="header-section">
    <div class="container">
        <div class="row align-items-center">
            <!-- Header Logo -->
            <div class="header-logo col-md-4 d-none d-md-block">

                              <!-- Start Logo Wrapper  -->
        <div class="site-title">
                      <a class="logo" href="{{url('/')}}" title="SeniorNews" rel="home">
                              <img src="{{asset('public/images/'.$Gsetting->logo)}}" alt="SeniorNews">
                          </a>
          
        </div>
        <!-- End Logo Wrapper -->
            </div>
            <!-- Header Banner -->
            <div class="header-banner col-md-8 col-12">
                <div class="banner">
                  <a href="https://vppy.app.link/Y18EuQfJLQ" target="_blank"><img src="{{asset('public/assets/senior_theme/images/hsUSA_ios_banner_728x90_B.gif')}}"></a>                   <!-- /51534170/SeniorNews_Header -->
<!-- <div id='div-gpt-ad-1539377253505-0' style='height:90px; width:728px;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1539377253505-0'); });
</script>
</div> -->
                  </div>
            </div>
            
        </div>
    </div>
</div><!-- Header End -->


  @include('home5.menu')
  @yield('cat_feature')

  
<footer class="footer-wrapper ">

<style type="text/css">@media only screen and (min-width: 1000px) and (max-width: 5000px){body.kc-css-system .kc-css-368281{width: 33.33%;}body.kc-css-system .kc-css-67374{width: 33.33%;}body.kc-css-system .kc-css-367384{width: 33.33%;}}body.kc-css-system .kc-css-37930{background: #1f2024;padding-top: 40px;}body.kc-css-system .kc-css-217933{background: #1f2024;padding-top: 20px;padding-bottom: 20px;}</style>
<section class="kc-elm kc-css-37930 kc_row custom-senior-footer">
<div class="kc-row-container  kc-container">
<div class="kc-wrap-columns">
<div class="kc-elm kc-css-368281 kc_col-sm-4 kc_column kc_col-sm-4">
<div class="kc-col-container">
<div class="widget widget_hashnews_description_widget kc-elm kc-css-409482">
<h4 class="widget-title">About Us</h4>
<div class="single_footer">
<div class="content fix">
{{$Gsetting->about}}
<ol class="footer-contact">
<li><i class="fa fa-home"></i>{{$Gsetting->address}}</li>
<li><i class="fa fa-envelope"></i>{{$Gsetting->email}}</li>
<li><i class="fa fa-headphones"></i>{{$Gsetting->phone}}</li>
</ol>
<div class="footer-social">
<p>             <a class="facebook" href="https://www.facebook.com/seniorcommunity" title="Facebook"><i class="fa fa-facebook"></i></a></p>
<p>               <a class="twitter" href="https://twitter.com/senioronline" title="Twitter"><i class="fa fa-twitter"></i></a></p>
<p>               <a class="google-plus" href="https://www.instagram.com/senior_com/" title="Instagram"><i class="fa fa-instagram"></i></a></p>
<p>               <a class="facebook" href="https://www.linkedin.com/company-beta/7589927/" title="Linkedin"><i class="fa fa-linkedin"></i></a></p>
<p>               <a style="background-color: #22b8cf;" class="google-plus" href="https://www.pinterest.com/seniorcom" title="Pinterest"><i class="fa fa-pinterest"></i></a></p>
<p>               <a class="google-plus" href="https://www.youtube.com/channel/UCSRS0tt6Pc8QolneZNgaykA?view_as=subscriber" title="Youtube"><i class="fa fa-youtube"></i></a> </p></div>
<p></p></div>
<p></p></div>
<p></p></div>
<p></p></div>
</div>
<div class="kc-elm kc-css-67374 kc_col-sm-4 kc_column kc_col-sm-4">
<div class="kc-col-container">
<div class="widget widget_execphp kc-elm kc-css-802068">
<h2 class="widgettitle">Latest Posts</h2>
<div class="execphpwidget">
<div class="footer-widget">
 @foreach($lastest as $n)

<div class="footer-widget-post">
  <div class="post-wrap">
     <a href="{{$n->link()}}" class="image"><br>
      <img width="300" height="200" src="{{asset('public/images/'.$n->image)}}" alt  sizes="(max-width: 300px) 100vw, 300px">               
      </a>
      <p></p>
    <div class="content">
    <h5 class="title"><a href="{{$n->link()}}">{{$n->title}}</a></h5>
  <p></p></div>
  <p></p>
  </div>

  <p></p>
</div>
 @endforeach

            
</div>
</div></div>
</div>
</div>
<div class="kc-elm kc-css-367384 kc_col-sm-4 kc_column kc_col-sm-4">
<div class="kc-col-container">
<div class="widget widget_hashnews_description_widget kc-elm kc-css-946781">
<h4 class="widget-title">General Information</h4>
<div class="single_footer">
<div class="content fix">
<ul>
<li><a href="{{url('/contact-us')}}">Contact Us</a></li>
<li><a href="{{url('/about-us')}}">Advertise With Us</a></li>
<li><a href="https://senior.com/wp-content/uploads/2017/07/Senior.com-Media-Kit-2017_r1-1.pdf-1.pdf" target="_blank" rel="noopener noreferrer">Download Media Kit</a></li>
<li><a href="http://seniornews.com/about/contact-writer/">Register to be a Writer</a></li>
<li><a href="{{url('/privacy')}}">Privacy Policy</a></li>
<li><a href="/terms-of-use/">Terms Of Use</a></li>
<li><a href="/copyright-and-dmca-policy/">Copyright and DMCA Policy</a></li>
</ul>
<div class="footer-social"></div>
<p></p></div>
<p></p></div>
<p></p></div>
<div class="widget widget_hashnews_description_widget kc-elm kc-css-559395">
<h4 class="widget-title">Press Releases</h4>
<div class="single_footer">
<div class="content fix">
<ul>
<li style="width: 100%;"><a href="/press-releases/">Press Releases</a></li>
<li style="width: 100%;"><a href="/press_release_p/">Partner Press Releases</a></li>
<li style="width: 100%;"><a href="/post-your-press-release/">Post Your Press Release</a></li>
</ul>
<div class="footer-social"></div>
<p></p></div>
<p></p></div>
<p></p></div>
<div class="widget widget_hashnews_description_widget kc-elm kc-css-243723">
<h4 class="widget-title">Affiliate Companies</h4>
<div class="single_footer">
<div class="content fix">
<ul>
<li style="width: 100%;"><a href="Https://senior.com" target="_blank">Senior.com</a></li>
<li style="width: 100%;"><a href="http://MobilitySenior.com" target="_blank">MobilitySenior.com</a></li>
<li style="width: 100%;"><a href="Https://MySmartActivate.com" target="_blank">MySmartActivate.com</a></li>
</ul>
<div class="footer-social"></div>
<p></p></div>
<p></p></div>
<p></p></div>
<p></p></div>
</div>
</div>
</div>
</section>
<section class="kc-elm kc-css-217933 kc_row">
<div class="kc-row-container  kc-container">
<div class="kc-wrap-columns">
<div class="kc-elm kc-css-322975 kc_col-sm-12 kc_column kc_col-sm-12">
<div class="kc-col-container">
<div class="kc-elm kc-css-678931 kc_text_block">
<p>{{$Gsetting->footer}}</p>
</div>
</div>
</div>
</div>
</div>
</section>
<div class="tptn_counter" id="tptn_counter_112565"></div><div class="swp-content-locator"></div>
</footer>

<div id="back-to-top"><i class="fa fa-angle-up"></i></div> 
  </div><!-- #page -->
</div>


        <script type="text/javascript"> 
            jQuery( document ).ready(function( $ ) {
  
                      $('.seniorn_latest_news .sidebar-block-wrapper .head .sidebar-tab-list a:nth-child(odd)').text('Latest News');
                      $('.seniorn_latest_news .sidebar-block-wrapper .head .sidebar-tab-list a:nth-child(even)').text('Popular News');

                      $.ajax({
                                type: 'POST',
                                beforeSend: function() {                                    
                                    $('.senior_feed_products').html('<div style="padding-left: 16%;"><img src="http://seniornews.com/wp-content/uploads/2018/10/loading.gif"  style="width: 225px;"/></div>');
                                },
                                url: 'http://seniornews.com/wp-admin/admin-ajax.php',
                                data: {"action": "get_senior_products" },
                                success: function(result){
                                   
                                   $('.senior_feed_products').html(result);
                                  
                            }
                      });                 
            });
        </script>
        <script>(function() {function addEventListener(element,event,handler) {
  if(element.addEventListener) {
    element.addEventListener(event,handler, false);
  } else if(element.attachEvent){
    element.attachEvent('on'+event,handler);
  }
}function maybePrefixUrlField() {
  if(this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
    this.value = "http://" + this.value;
  }
}

var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
if( urlFields && urlFields.length > 0 ) {
  for( var j=0; j < urlFields.length; j++ ) {
    addEventListener(urlFields[j],'blur',maybePrefixUrlField);
  }
}/* test if browser supports date fields */
var testInput = document.createElement('input');
testInput.setAttribute('type', 'date');
if( testInput.type !== 'date') {

  /* add placeholder & pattern to all date fields */
  var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
  for(var i=0; i<dateFields.length; i++) {
    if(!dateFields[i].placeholder) {
      dateFields[i].placeholder = 'YYYY-MM-DD';
    }
    if(!dateFields[i].pattern) {
      dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
    }
  }
}

})();</script><script type="text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/seniornews.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */
</script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/scripts.js')}}"></script>
<script type="text/javascript">
/* <![CDATA[ */
var wpmm_object = {"ajax_url":"http:\/\/seniornews.com\/wp-admin\/admin-ajax.php","wpmm_responsive_breakpoint":"767px","wpmm_disable_mobile":"false"};
/* ]]> */
</script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/wpmm.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/silicon-counters.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/script.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/waypoints.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/imagesloaded.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/isotope.pkgd.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/jquery.magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/wow-min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/navigation.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/skip-link-focus-fix.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/jquery.meanmenu.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/retina.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/jquery.onepage.nav.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/jquery-advanced-news-ticker.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/jquery.simpleWeather.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/jquery.nicescroll.js')}}"></script>
<script type="text/javascript">
/* <![CDATA[ */
var mobile_menu_data = {"menu_width":"991","related_post_number":"2"};
/* ]]> */
</script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/main.js')}}"></script>
<script type="text/javascript">
/* <![CDATA[ */
var advadsTracking = {"ajaxurl":"http:\/\/seniornews.com\/wp-admin\/admin-ajax.php?action=advanced-ads-tracking-track","method":null,"blogId":"1"};
/* ]]> */
</script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/script.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/kingcomposer.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/wp-embed.min.js')}}"></script>
<script type="text/javascript">
/* <![CDATA[ */
var mc4wp_forms_config = [];
/* ]]> */
</script>
<script type="text/javascript" src="{{asset('public/assets/senior_theme/js/forms-api.min.js')}}"></script>
<!--[if lte IE 9]>
<script type='text/javascript' src='http://seniornews.com/wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js?ver=4.2.5'></script>
<![endif]-->
<script>window.advads_placement_tests = [];
window.advads_passive_ads = {};
window.advads_passive_groups = {};
window.advads_passive_placements = {};
window.advads_ajax_queries = [];
window.advads_has_ads = [["112517","ad",null],["112418","ad",null],["112503","ad",null]];
window.advads_js_items = [];
( window.advanced_ads_ready || jQuery( document ).ready ).call( null, function() {if ( window.advanced_ads_pro ) {advanced_ads_pro.process_passive_cb();} else if ( window.console && window.console.log ) {console.log('Advanced Ads Pro: cache-busting can not be initialized');} });</script><script type="text/javascript">var swpFloatBeforeContent = false;var swpClickTracking = false;</script><script type="text/javascript">var advads_tracking_ads = {};var advads_tracking_urls = [];var advads_gatracking_uids = [];var advads_tracking_methods = [];var advads_tracking_parallel = [];var advads_tracking_linkbases = [];var advads_gatracking_allads = [];var advads_gatracking_anonym = true;</script>
</body>
</html>
