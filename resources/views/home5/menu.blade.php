<div class="menu-section bg-dark ">
  <div class="header-area">
    <div class="container">
      <div class="row">
        <div class="col-12">

          <div class="menu-section-wrap">
            <div class="main-menu float-left">
              <nav>
                <ul id="menu-main-menu" class="menu"><li id="menu-item-111546" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-111546"><a href="{{url('/')}}">Home</a></li>
              @foreach($catMenu as $cat)
              <li id="menu-item-111336" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-111336"><a href="{{url('/v/'.$cat->id.'/'.$cat->slug)}}">{{$cat->name}}</a>
                  <ul class="sub-menu">
                   @foreach($cat->children as $child)
                      <li id="menu-item-{{$child->id}}" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-{{$child->id}}">
                        <a href="{{url('/v/'.$child->id.'/'.$child->slug)}}">{{$child->name}}</a></li>
                   @endforeach

                  </ul>
              </li>
              @endforeach



<li id="menu-item-112076" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-112076"><a href="http://seniornews.com/authors/">Authors</a>
<ul class="sub-menu">
  <li id="menu-item-112093" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-112093"><a href="http://seniornews.com/author/andrew-vertson/">Andrew Vertson</a>
  </li>
</ul>
</li>
<li id="menu-item-111542" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-111542"><a target="_blank" href="https://senior.com/shop/">Shop</a></li>
<li id="menu-item-111545" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-111545"><a href="http://seniornews.com/contact/">Contact</a></li>
</ul>             </nav>
            </div>

            <div class="mobile-logo d-none d-block d-md-none">
                              <a href="http://seniornews.com/" title="SeniorNews" rel="home">
                                      <img src="images/logo.png" alt="SeniorNews">
                                  </a>
                          </div>

            <!-- Header Search -->
                        <div class="header-search float-right">
                            <button class="header-search-toggle"><i class="fa fa-search"></i></button>
                            <div class="header-search-form">
                                <form id="search" action="{{url('/search')}}" method="GET">
                                    <input type="text" name="searchinput" placeholder="Search Here">
                                </form>
                            </div>
                        </div>

                        <!-- Mobile Menu  
            <div class="mobile-menu"></div>-->

          </div>
        </div>
      </div>
      
    </div>
  </div>  
</div>


          
