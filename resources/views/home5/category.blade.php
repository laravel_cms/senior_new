@extends('welcome5')

@section('cat_feature')

<div id="content" class="site-content">
      <div class="page-wrapper blog-story-area mt-4">
  <div class="container">
    <div class="row">

            <!-- single blog right sidebar start -->
      <div class="col-lg-8 col-12 mb-50 border_none_blog">
      	@foreach($news as $n)

<div id="post-{{$n->id}}" class="post-85759 post type-post status-publish format-standard has-post-thumbnail hentry category-dating tag-dating tag-online-dating tag-senior-dating tag-senior-relationships">
    	<div class="single-blog mb-50">
    		<div class="blog-wrap dating">
                <!-- Meta -->
                <div class="meta fix">
                                                    <a href="#" class="meta-item category dating">{{$cat->name}}</a>
                                                <a href="{{$n->user->link()}}" class="meta-item author"><img alt="mm" src="{{$n->user->get_gravatar()}}" class="avatar avatar-90 photo" height="90" width="90">{{$n->user->full_name()}}</a>
                    <!-- <span class="meta-item date"><i class="fa fa-clock-o"></i>December 1, 2018</span> -->
                </div>
                <!-- Title -->
                                    <h4 class="title"><a href="{{$n->link()}}">{{$n->title}}</a></h4>
                                <!-- Image -->
                                	<a class="image" href="{{$n->link()}}"><img width="1140" height="660" src="{{asset('public/images/'.$n->image)}}" class="attachment-hashnews_blog_img size-hashnews_blog_img wp-post-image" alt=""  sizes="(max-width: 1140px) 100vw, 1140px"></a>
                                <!-- Content -->
                <div class="content">
                    <!-- Description -->
                    <p>{!! str_limit($n->details,200) !!}</p>

                    <!-- Read More -->
                    <a href="{{$n->link()}}" class="read-more">Continue reading</a>
                </div>
            </div>
        </div>
    </div>

		@endforeach


            
            
      </div>
      
      @include('home5.content_left')

      <!--single blog right sidebar end -->
      
    </div>
  </div>
</div>

  </div>



@stop


