@extends('welcome5')

@section('cat_feature')

<div id="content" class="site-content">
      <div class="page-wrapper blog-story-area mt-4">
  <div class="container">
    <div class="row">

            <!-- single blog right sidebar start -->
      <div class="col-lg-8 col-12 mb-50">
        
                        <div class="row">
                <div class="col-sm-12">
                    <div class="hd-info">
                        <h3 style=" border-bottom: 3px solid #ccc; padding-bottom: 8px;"> About the Author <a href="{{url('/author/all')}}" class="btn btn-primary btn-sm  ml-2">View All Authors</a> </h3>
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="auther-holder">
                      <div class="row">
                          <div class="auther-imgbox  col-sm-3">
                <figure>
                                    <img src="{{$author->get_gravatar()}}" alt="" width="150" height="150" class="rounded-circle">
                </figure>
              </div>
                                      
                        <div class="auther-nfobox col-sm-9">
                            <h3 class="mb-0">{{$author->first_name.$author->last_name}}  </h3>
                            <strong>  {{$author->job}}  </strong>
                                  <p>{{$author->info}}</p>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-sm-12">
                  <div class="e-fashionHolder">
                    <div class="row">
                          @foreach($news as $n)
             
                          <div class="col-sm-4">
                <div class="e-fashionblog">
                  <div class="e-shop-imgbox">
                    <a href="{{$n->link()}}">
                      </a><figure>
                        <a href="{{$n->link()}}">
                        <img width="240" height="194" src="{{asset('public/images/'.$n->image)}}" class="img-responsive wp-post-image" alt="Best Guide to Bathroom Safety for Seniors"  sizes="(max-width: 240px) 100vw, 240px">
                        </a><a href="{{$n->link()}}" class="shop-link">
                          <figcaption class="e-shopListover">
                            <ul class="">
                              <li>{{$n->title}}</li>
                            </ul>
                          </figcaption>
                        </a>
                      </figure>
                    
                  </div>
                  <p>{!! str_limit($n->details,200) !!}</p>
                </div>
              </div>
                @endforeach
                <div class="row">
        <div class="col-sm-12">
            <div class="fashion-pager">
                                <ul class="fashionpager-list clearfix">
                                            <li class="pager"><a href="http://seniornews.com/author/johnson/page/2/">Next »</a></li>
                                    </ul>
            </div>
        </div>
    </div>
                      </div>
        </div>
               </div>
             </div>  
            
      </div>
      
      @include('home5.content_left')

      <!--single blog right sidebar end -->
      
    </div>
  </div>
</div>

  </div>



@stop


