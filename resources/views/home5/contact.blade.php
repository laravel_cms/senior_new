@extends('welcome5')

@section('cat_feature')
	<div id="content" class="site-content">
				<div class="page-wrapper">
		<div class="kc_clfw"></div><section class="kc-elm kc-css-243355 kc_row no_space contract_area mb-50"><div class="kc-row-container  kc-container"><div class="kc-wrap-columns"><div class="kc-elm kc-css-968243 kc_col-sm-8 kc_column kc_col-sm-8"><div class="post-block-wrapper kc-col-container">           	<div class="kc-elm kc-css-36237 contact_info">

                <div class="head"><h4 class="title">Contact Information.</h4></div>                <!-- Post Block Body Start -->
                	                <div class="body">
	                    <div class="contact-info row">
	                    			                        <div class="single-contact text-center col-md-4">
		                            <i class="fa-home"></i>
		       @if($Gsetting->address != null)<p>{!! $Gsetting->address !!}</p>@endif
		                        </div>
		              <div class="single-contact text-center col-md-4">
		                            <i class="fa-envelope"></i>
		        @if($Gsetting->email != null)
				<p>{!! $Gsetting->email !!}</p>
					@endif
</p>
		                        </div>
		          <div class="single-contact text-center col-md-4">
		                            <i class="fa-headphones"></i>
		       @if($Gsetting->phone != null)
				<p>{!! $Gsetting->phone !!}</p>
				@endif
		                        </div>
		                    	                    </div>
	                </div><!-- Post Block Body End -->
	            
			</div>
	        <div class="kc-contact-form7 kc-elm kc-css-258155"><h2>Send us a Message.</h2><div role="form" class="wpcf7" id="wpcf7-f110276-p110233-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form  novalidate="novalidate" action="{{route('contact')}}" method="post" role="form" class="wpcf7-form" >
		{{csrf_field()}}


<div class="body">
<div class="contact-form row pt-1">
<div class="col-md-6 col-12 mb-20">
           <label for="name">Name <sup>*</sup></label><span class="wpcf7-form-control-wrap text-790"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span>
      </div>
<div class="col-md-6 col-12 mb-20">
           <label for="email">Email <sup>*</sup></label><span class="wpcf7-form-control-wrap email-15"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span>
       </div>
<div class="col-12 mb-20">
           <label for="PhoneNumber">Phone Number </label><span class="wpcf7-form-control-wrap phone-number"><input type="text" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span>
       </div>
<div class="col-12 mb-20">
           <label for="PhoneNumber">Subject </label><span class="wpcf7-form-control-wrap phone-number"><input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span>
       </div>
<div class="col-12 mb-20">
           <label for="message">Message <sup>*</sup></label><span class="wpcf7-form-control-wrap textarea-350"><textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea></span>
       </div>
<div class="col-12">
            <input type="submit" value="send message" class="wpcf7-form-control wpcf7-submit"><span class="ajax-loader"></span>
       </div>
<div class="col-md-12 col-12 mt-4">
If you are interested in being a content contributor to SeniorNews.com and Senior.com, <a href="https://senior.com/about/contact-writer/" target="_blank">click here</a>.
</div>
<p></p></div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div>
</form>
</div></div></div></div>
<div class="kc-elm kc-css-655703 kc_col-sm-4 kc_column kc_col-sm-4"><div class="kc-col-container"><div class="widget widget_hashnews_newsletter_widget kc-elm kc-css-952748">					<div class="sidebar-subscribe">
						<h4>Subscribe Now To <br>Our eNewsLetter:</h4>
<p>Plus receive discounts from our partners!</p>
						<script>(function() {
	if (!window.mc4wp) {
		window.mc4wp = {
			listeners: [],
			forms    : {
				on: function (event, callback) {
					window.mc4wp.listeners.push({
						event   : event,
						callback: callback
					});
				}
			}
		}
	}
})();
</script><!-- MailChimp for WordPress v4.2.5 - https://wordpress.org/plugins/mailchimp-for-wp/ -->

<form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-47" method="post" data-id="47" data-name="Subscribe Form"><div class="mc4wp-form-fields">

<input type="email" name="EMAIL" placeholder="Your email address" required="">
<input type="submit" value="Sign up">
</div><label style="display: none !important;">Leave this field empty if you're human: <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off"></label><input type="hidden" name="_mc4wp_timestamp" value="1553337977"><input type="hidden" name="_mc4wp_form_id" value="47"><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1"><div class="mc4wp-response"></div></form><!-- / MailChimp for WordPress Plugin -->					</div>
				</div></div></div></div></div></section><div class="tptn_counter" id="tptn_counter_110233"></div><div class="swp-content-locator"></div>	</div><!-- #primary -->

	</div>

@stop
