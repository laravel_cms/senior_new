@extends('welcome5')

@section('cat_feature')

<div id="content" class="site-content">
      <div class="page-wrapper blog-story-area mt-4">
  <div class="container">
    <div class="row">

            <!-- single blog right sidebar start -->
      <div class="col-lg-8 col-12 mb-50">
        <div class="blog-wrapper blog-single">
    
       

        <!-- Single Blog Start -->
        <div class="single-blog mb-50 post-94005 post type-post status-publish format-standard has-post-thumbnail hentry category-age-in-place tag-aging tag-aging-in-place tag-elder-care tag-home-care tag-senior-care tag-visiting-angels">
            <div class="blog-wrap">

                <!-- Meta -->
                <div class="meta fix">
                     <a href="#" class="meta-item category age-in-place">{{$new->category->name}}</a>
                    <a href="http://seniornews.com/author/visiting-angels/" class="meta-item author"><img alt="mm" src="http://seniornews.com/wp-content/uploads/2016/08/visiting-angels-150x150.jpg" class="avatar avatar-90 photo" height="90" width="90">{{$new->user_name}}</a>
                    <!-- <span class="meta-item date"><i class="fa fa-clock-o"></i>December 29, 2018</span> -->

                    <span class="meta-item comments"><i class="fa fa-comments"></i>(<a href="http://seniornews.com/5-biggest-reasons-aging-place/#respond" class="post-comment">{{ $comments->count() }} </a>)</span>

                    <span class="meta-item view"><i class="fa fa-eye"></i>({{$new->hit_count}})</span>
                </div>

                <!-- Title -->
                                    <h3 class="title">{{$new->title}}</h3>
                                
                                                            <div class="image"><img width="730" height="400" src="{{asset('public/images/'.$new->image)}}" alt="5 Biggest Reasons for Aging in Place"></div>
                    
                <!-- Content -->
                <div class="content">
                     {!! $new->details !!}
               </div>

                <div class="tags-social float-left">
                                            <div class="tags float-left">
                            <i class="fa fa-tags"></i> 
                           <?php $source=  $new->source; $tags = explode(',', $source); ?>
                            @foreach($tags as $tag)
                              @if($tag)<a href="#"> # {{$tag}} </a>@endif
                            @endforeach


                                                  </div>
                    
                                            <div class="blog-social float-right">
                            <a href="https://www.facebook.com/sharer/sharer.php?u=http://seniornews.com/5-biggest-reasons-aging-place/" class="facebook"><i class="fa fa-facebook"></i></a><a href="https://twitter.com/share?http://seniornews.com/5-biggest-reasons-aging-place/&amp;text=5%20Biggest%20Reasons%20for%20Aging%20in%20Place" class="twitter"><i class="fa fa-twitter"></i></a><a href="https://plus.google.com/share?url=http://seniornews.com/5-biggest-reasons-aging-place/" class="google-plus"><i class="fa fa-google-plus"></i></a><a href="http://www.linkedin.com/shareArticle?url=http://seniornews.com/5-biggest-reasons-aging-place/&amp;title=5%20Biggest%20Reasons%20for%20Aging%20in%20Place" class="linkedin"><i class="fa fa-linkedin"></i></a><a href="https://pinterest.com/pin/create/bookmarklet/?url=http://seniornews.com/5-biggest-reasons-aging-place/&amp;description=5%20Biggest%20Reasons%20for%20Aging%20in%20Place&amp;media=http://seniornews.com/wp-content/uploads/2017/05/Age-in-Place-Hands.jpg" class="pinterest"><i class="fa fa-pinterest"></i></a>                        </div>
                    
                </div>

            </div>
        </div><!-- Single Blog End -->
        
          <div class="space no-top comments-sec">
    <div class="single-title">
      <h4><i class="fa fa-comments"></i>{{ $comments->count() }}  Comments.</h4>
    </div>
    <ul>
      @foreach($comments as $comment)
      <li>
        <div class="comment">
          @if(!empty($comment->user->email))
          <img src="{{$comment->user->get_gravatar()}}" width="70px">
           @endif
          <div class="comment-detail">
            <h4><a title="" href="#">{{ $comment->user->first_name }} {{ $comment->user->first_name }}</a></h4><span>{{ date("d F Y - h:ia", strtotime($comment->created_at)) }}</span>
            <p>{!! $comment->comment !!}</p>
            @if(Sentinel::check())
            <!-- <a title="Replay" class="reply" data-id="{{ $comment->id }}"><i class="fa fa-reply"></i>Reply</a> -->
            @endif
          </div>
        </div>
        <div id="reply_form_{{ $comment->id }}"></div>

      </li>
      @endforeach
    </ul>
  </div>
  <div class="contact-form">
    <div class="single-title">
      <h4>leave a comment</h4>
    </div>

    <div class="row">
      <div class="col-sm-12">
        @if (!empty(Session::get('message')))
        <p style="color: #5cb85c">{{ Session::get('message') }}</p>
        @elseif (!empty(Session::get('exception')))
        <p style="color: #d9534f">{{ Session::get('exception') }}</p>
        @endif
      </div>
      @if(Sentinel::check())
      <form data-parsley-validate action="{{ route('commentRoute', $new->id) }}" method="post">
        {{ csrf_field() }}
        <div class="col-sm-12{{ $errors->has('comment') ? ' has-error' : '' }}">
          <label>Comment <span class="required">*</span></label>
          <textarea name="comment" cols="30" rows="10" required></textarea>
          @if ($errors->has('comment'))
          <span class="help-block">
            <strong>{{ $errors->first('comment') }}</strong>
          </span>
          @endif
        </div>
        <div class="col-sm-12">
          <button type="submit">Post Comment <i class="fa fa-angle-right"></i></button>
        </div>
      </form>
      @else
      <div class="col-sm-12">
        <p>You must login to post a comment. Already Member <a href="{{ route('login') }}">Login</a> | New <a href="{{ route('register') }}">Register</a></p>
      </div>
      @endif
    </div>
  </div>
        <!-- Previous & Next Post Start -->
                    <div class="post-nav mb-50">
                                    <a href="http://seniornews.com/seniors-benefit-pet-therapy/" class="prev-post"><span class="next_prev_text">previous post</span><span class="next_prev_title">How Seniors Benefit from Pet Therapy</span></a>
                                    <a href="http://seniornews.com/aging-friends-fills-void-alone/" class="next-post"><span class="next_prev_text">next post</span><span class="next_prev_title">How Aging with Friends Fills the Void of Being Alone   &nbsp;&nbsp;&nbsp;</span></a>
                            </div><!-- Previous & Next Post End -->
                
    <!-- Start Author Info -->
      <!-- End Author Info -->
        </div>
        
            

          
  <div class="row">
                <div class="col-sm-12">
                    <div class="hd-info">

                        <h4 style=" border-bottom: 3px solid #ccc; padding-bottom: 8px;"> About the Author </h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="auther-holder">
                      <div class="row">
                          <div class="auther-imgbox  col-sm-3">
                <figure>
                                    <img src="http://seniornews.com/wp-content/uploads/2016/08/visiting-angels-150x150.jpg" alt="" width="150" height="150" class="rounded-circle">
                </figure>
              </div>
                                      
                        <div class="auther-nfobox col-sm-9">
                            <h3>Richard Bitner</h3>
                            
                            <p>Visiting Angels is a national, private duty network of senior care agencies. We are proud to be the nation’s leading provider of non-medical at home care services. With our elder care services, seniors can remain independent and live safely at home. Our senior care services include Social Care, Dementia Care, Alzheimer’s Care, End of Life Care, Companion Care, Private Duty Care, care to prevent hospital readmission, and so much more.</p>
<p>Compassionate, dignified at home senior care is close to home when you connect with your local Visiting Angels office by calling 800-365-4189.</p>
                            
                            <a href="http://seniornews.com/author/visiting-angels/" class="btn btn-primary">View All Articles</a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>





            
            
      </div>
      
      @include('home5.content_left')

      <!--single blog right sidebar end -->
      
    </div>
  </div>
</div>

  </div>



@stop


