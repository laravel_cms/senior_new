@extends('welcome5')

@section('cat_feature')

<div id="content" class="site-content">
      <div class="page-wrapper blog-story-area mt-4">
  <div class="container">
    <div class="row">

            <!-- single blog right sidebar start -->
      <div class="col-lg-8 col-12 mb-50">
        

            <div class="row ">
                <div class="col-sm-12">
                    <div class="hd-info">
                        <h2 style=" border-bottom: 3px solid #ccc; padding-bottom: 8px;"> Authors </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="auther-holder">
                      <div class="e-fashionHolder">
                      <div class="row">
                  @foreach($authors as $author)

                    <div class="col-sm-4">
              <div class="e-fashionblog">
                <div class="e-shop-imgbox">
                  <a href="http://seniornews.com/author/andrew-vertson/">
                    </a><figure><a href="http://seniornews.com/author/andrew-vertson/">
                      <img src="{{$author->get_gravatar()}}" alt="">

                      </a><a href="{{$author->link()}}" class="shop-link">
                        <figcaption class="e-shopListover">
                          <ul class="">
                            <li style="font-weight: normal;">
                              <strong>{{$author->first_name}} {{$author->last_name}}</strong>
                                <br>
                                {{$author->job}}
                            </li>
                          </ul>
                        </figcaption>
                      </a>
                    </figure>
                  
                </div>
              </div>
            </div>
               @endforeach

                    </div>
                    </div>
                    </div>
                </div>
            </div>
            
      </div>
      
      @include('home5.content_left')

      <!--single blog right sidebar end -->
      
    </div>
  </div>
</div>

  </div>



@stop


