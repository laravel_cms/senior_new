@extends('welcome5')

@section('cat_feature')


        <div id="content" class="site-content">
                <div class="page-wrapper">
        <div class="kc_clfw"></div>

       <section class="kc-elm kc-css-460748 kc_row hero-section mt-30 mb-50"><div class="kc-row-container  kc-container"><div class="kc-wrap-columns"><div class="kc-elm kc-css-566380 kc_col-sm-3 kc_column kc_col-sm-3"><div class="kc-col-container">    
   @foreach($slider2 as $n)
        
    <div class="kc-elm kc-css-581137 ">
        
        <div class="order-lg-1">
            
                            <!-- Overlay Post Start -->
                <div class="post post-overlay hero-post ">
                    <div class="post-wrap">
                                            <!-- Image -->
                        <div class="image">
                            <img width="270" height="205" src="{{asset('public/images/'.$n->image)}}" class="attachment-hashnews_size_270x205 size-hashnews_size_270x205 wp-post-image" alt="{{$n->title}}"  sizes="(max-width: 270px) 100vw, 270px">                        </div>
                
                        <!-- Category -->
                                                            <a href="{{$n->link()}}" class="category">{{$n->category->name}}</a>
                                                        <!-- Content -->
                        <div class="content">
                            <h4 class="title"><a href="{{$n->link()}}">{{$n->title}}</a></h4>
                            <div class="meta fix">
                                <span class="meta-item date"><i class="fa fa-clock-o"></i>
                                    {{$n->updated_at->toFormattedDateString()}}                               </span>
                            </div>
                        </div>
                    </div>
                </div><!-- Overlay Post End -->
            
                    </div>

    </div>
     @endforeach
    </div></div><div class="kc-elm kc-css-360630 kc_col-sm-6 kc_column kc_col-sm-6"><div class="kc-col-container">  
    <div class="kc-elm kc-css-696207 home_pg_mid">
        
        <div class="order-lg-2">
            <div class="post-carousel-1">
                            @foreach($slider as $n)
                            <!-- Overlay Post Start -->
                <div class="post post-overlay hero-post post-large">
                    <div class="post-wrap">
                                            <!-- Image -->
                        <div class="image">
                            <img width="484" height="260" src="{{asset('public/images/'.$n->image)}}" class="attachment-hashnews_size_585x445 size-hashnews_size_585x445 wp-post-image" alt  sizes="(max-width: 484px) 100vw, 484px">  
                         </div>
                
                        <!-- Category -->
                         
                           <a href="{{$n->link()}}" class="category">{{$n->category->name}}</a>

                             <!-- Content -->
                        <div class="content">
                            <h4 class="title"> <a href="{{$n->link()}}">{{$n->title}}</a></h4>
                            <div class="meta fix">
                                <span class="meta-item date"><i class="fa fa-clock-o"></i>
                                    {{$n->updated_at->toFormattedDateString()}} 
                                </span>
                            </div>
                        </div>
                    </div>
                </div><!-- Overlay Post End -->
                 @endforeach
            
            </div>      </div>

    </div>
    </div></div><div class="kc-elm kc-css-64397 kc_col-sm-3 kc_column kc_col-sm-3"><div class="kc-col-container">   

    @foreach($slider3 as $n)
              
        
    <div class="kc-elm kc-css-465456 ">
        
        <div class="order-lg-1">
            
                            <!-- Overlay Post Start -->
                <div class="post post-overlay hero-post ">
                    <div class="post-wrap">
                                            <!-- Image -->
                        <div class="image">
                            <img width="270" height="205" src="{{asset('public/images/'.$n->image)}}" class="attachment-hashnews_size_270x205 size-hashnews_size_270x205 wp-post-image" alt=""  sizes="(max-width: 270px) 100vw, 270px">
                        </div>
                
                        <!-- Category -->
                             <a href="{{$n->link()}}" class="category health-insurance">{{$n->category->name}}</a>
                                                        <!-- Content -->
                        <div class="content">
                            <h4 class="title">
                             <a href="{{$n->link()}}">{{$n->title}}</a>
                            </h4>
                            <div class="meta fix">
                                <span class="meta-item date"><i class="fa fa-clock-o"></i>
                                   {{$n->updated_at->toFormattedDateString()}}                              </span>
                            </div>
                        </div>
                    </div>
                </div><!-- Overlay Post End -->
            
                    </div>

    </div>
    @endforeach
   
    </div></div></div></div></section>



       <section class="kc-elm kc-css-89402 kc_row no_space"><div class="kc-row-container  kc-container  post-section"><div class="kc-wrap-columns">

        <div class="kc-elm kc-css-839854 kc_col-sm-8 kc_column kc_col-sm-8">
            <div class="kc-col-container">    
                <div class="kc-elm kc-css-4868 ">
        <div class="post-block-wrapper">

            <!-- Post Block Head Start -->
            <div class="head">
                <h4 class="title"> Featured News</h4>                   <!-- Tab List Start -->
                    <ul class="post-block-tab-list nav d-none d-md-block">
                         <?php $i=0;?>
                         @foreach($featuredCateArr as $cate)

                            <li><a class="<?php if($i==0) echo "active" ?>" data-toggle="tab" href="#tab{{$cate['cate_id']}}">{{$cate['name']}}</a></li>
                            <?php $i++;?>
                         @endforeach
                       
                        </ul>                   <!-- Tab List End -->

                    <!-- Tab List Start -->
                    <ul class="post-block-tab-list feature-post-tab-list nav d-sm-block d-md-none">
                        <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">Category</a>
                            <!-- Dropdown -->
                            <ul class="dropdown-menu">
                               <?php $i=0;?>
                                 @foreach($featuredCateArr as $cate)

                                    <li><a class="<?php if($i==0) echo "active" ?>" data-toggle="tab" href="#tab{{$cate['cate_id']}}">{{$cate['name']}}</a></li>
                                    <?php $i++;?>
                                 @endforeach
                            </ul>
                        </li>
                    </ul><!-- Tab List End -->
                
            </div><!-- Post Block Head End -->
            <div class="body pb-0">
                 <div class="tab-content">
                    <?php $i=0;?>
                                 @foreach($featuredCateArr as $cate)
                        <?php $post = $cate['first_post'];?>         
                    <div class="tab-pane fade  <?php if($i==0) echo "active show" ?>" id="tab{{$cate['cate_id']}}">
                            <div class="row">
                                <div class="col-md-6 col-12 mb-20">
                                    <!-- Post Start -->
                                    <div class="post feature-post post-separator-border ">
                                        <div class="post-wrap">
                                            
                                            <a class="image" href="{{url('/article/'.$post->id.'/'.$post->slug)}}">
                                                <img width="371" height="221" src="{{asset('public/images/'.$post->image)}}" class="attachment-hashnews_size_371x221 size-hashnews_size_371x221 wp-post-image" alt=""  sizes="(max-width: 371px) 100vw, 371px">
                                            </a>    
                                            <div class="content">
                                                <h4 class="title">
                                                    <a href="{{url('/article/'.$post->id.'/'.$post->slug)}}">
                                                    {{$post->title}}        
                                                    </a>
                                                </h4>
                                                <div class="meta fix">
                                       <a href="{{url('/author/'.$post->user->id.'/'.$post->user->first_name.'_'.$post->user->last_name)}}" class="meta-item author">
                                        <i class="fa fa-user"></i>{{$post->user->first_name}} {{$post->user->last_name}}   </a>
                                             <span class="meta-item date"><i class="fa fa-clock-o"></i>{{$post->updated_at->toFormattedDateString()}}   </span>
                                          <!--  <span class="meta-item comments"><i class="fa fa-comments"></i>(<span class="post-comment">Comments off</span>)</span> -->
                                              </div>
                                              <p>{!! str_limit($post->details,200) !!}</p>
                                            </div>
                                        
                                        </div>
                                    </div><!-- Post End -->
                                </div>
                                <div class="col-md-6 col-12 mb-20">
                                     @foreach($cate['list_post'] as $n)
                                    <!-- Post Start -->
                                    <div class="post feature-post post-separator-border post-small post-list">
                                        <div class="post-wrap">
                                            
                                            <a class="image" href="{{$n->link()}}">
                                                <img width="124" height="94" src="{{asset('public/images/'.$n->image)}}" class="attachment-hashnews_size_124x94 size-hashnews_size_124x94 wp-post-image" alt="{{$n->title}}" sizes="(max-width: 124px) 100vw, 124px">
                                            </a>    
                                            <div class="content">
                                                <h4 class="title">
                                                    <a href="{{$n->link()}}">{{$n->title}}</a>
                                                </h4>
                                                <div class="meta fix">
                                                     <span class="meta-item date"><i class="fa fa-clock-o"></i>  {{$n->updated_at->toFormattedDateString()}}       
                                                     </span>
                                                </div>
                                            </div>
                                        
                                        </div>
                                    </div><!-- Post End -->
                                    @endforeach  
                                </div>
                                </div><!--  End row -->
                                     </div>
                                     <?php $i++;?>
                           @endforeach  
                        </div>
                    </div><!--  End body p 0 --> 
                </div>
            </div>
        </div>
    </div>
    <div id="senior_products" class="kc-elm kc-css-820959 kc_col-sm-4 kc_column kc_col-sm-4"><div class="kc-col-container">
        <div class="widget widget_execphp kc-elm kc-css-155375">
            <h2 class="widgettitle">Our Partner Best Product Offers</h2>         <div class="execphpwidget"><div style="margin: 10%;"><a href="https://vppy.app.link/Y18EuQfJLQ" target="_blank"><img src="http://seniornews.com/wp-content/uploads/2019/02/hsUSA_ios_banner_300x250_C.gif"></a></div></div>
        </div></div></div></div></div></section>


        <section class="kc-elm kc-css-882802 kc_row no_space"><div class="kc-row-container  kc-container"><div class="kc-wrap-columns"><div class="kc-elm kc-css-986219 kc_col-sm-4 kc_column kc_col-sm-4"><div class="kc-col-container">   
    <div class="kc-elm kc-css-154885 ">
        
        <div class="post-block-wrapper">

            <div class="head"><h4 class="title">{{$home1CateArr['name']}}</h4></div>            <div class="body ">
                <div class="slick-row-active row-post-carousel post-block-carousel education-post-carousel" data-slick-items="1" data-slick-rows="4" data-slick-arrows="true" data-slick-autoplay="false" data-slick-autoplayspeed="5000" data-slick-loop="false" data-slick-desktop="1" data-slick-tablate="1">    
                              @foreach($home1CateArr['list_post'] as $n)
                        
                        <div class="post post-small post-list education-post">
                        <div class="post-wrap">
                            <a class="image" href="{{$n->link()}}"
                             tabindex="0"><img width="124" height="94" src="{{asset('public/images/'.$n->image)}}" class="attachment-hashnews_size_124x94 size-hashnews_size_124x94 wp-post-image" alt=""   sizes="(max-width: 124px) 100vw, 124px" 100vw, 124px"></a>
                            <div class="content">
                                <h5 class="title"><a href="{{$n->link()}}">{{$n->title}}</a></h5>
                                <div class="meta fix">
                                    <span class="meta-item date"><i class="fa fa-clock-o"></i>{{$n->updated_at->toFormattedDateString()}} </span>
                                </div>

                            </div>
                        </div>
                    </div>
                    @endforeach  

                                   
                    
                </div>          </div>

        
        </div>

    </div>
    </div></div><div class="kc-elm kc-css-819780 kc_col-sm-4 kc_column kc_col-sm-4"><div class="kc-col-container">  
    <div class="kc-elm kc-css-417795 home-and-family">
        
        <div class="post-block-wrapper">

            <div class="head"><h4 class="title">{{$home2CateArr['name']}}</h4></div>         <div class="body ">
                <div class="slick-row-active post-block-carousel gadgets-post-carousel" data-slick-items="1" data-slick-rows="2" data-slick-arrows="true" data-slick-autoplay="false" data-slick-autoplayspeed="5000" data-slick-loop="false" data-slick-desktop="1" data-slick-tablate="1">   @foreach($home2CateArr['list_post'] as $n)
                        <div class="post gadgets-post" style="width: 100%; display: inline-block;">
                        <div class="post-wrap">
                            <a class="image" href="http://seniornews.com/how-to-make-end-of-life-plans/" tabindex="0"><img width="370" height="168" src="{{asset('public/images/'.$n->image)}}" class="attachment-hashnews_size_370x168 size-hashnews_size_370x168 wp-post-image" alt=""></a>
                            <div class="content">
                                <h4 class="title"><a href="{{$n->link()}}">{{$n->title}}</a></h4>
                                                            </div>
                        </div>
                    </div>

                    @endforeach  
                    

                </div>          </div>

        
        </div>

    </div>
    </div></div><div class="kc-elm kc-css-287270 kc_col-sm-4 kc_column kc_col-sm-4"><div class="kc-col-container"><div class="widget widget_execphp kc-elm kc-css-204885">          <div class="execphpwidget"><div class="kc-col-container">   
    <div class="kc-elm kc-css-568909 seniorn_latest_news">
        <div class="sidebar-block-wrapper">
        <!-- Sidebar Block Head Start -->
        <div class="head education-head">
            <div class="sidebar-tab-list education-sidebar-tab-list nav">
                                <a class="active" data-toggle="tab" href="#tablatestnews" aria-expanded="false">Latest News</a>
                                <a class data-toggle="tab" href="#tabpopularnews" aria-expanded="true">Popular News</a>
                            </div>
        </div><!-- Sidebar Block Head End -->

        <div class="body">

            <div class="tab-content">
                 <div class="tab-pane fade active show" id="tablatestnews" aria-expanded="true">
  @foreach($lastest as $n)
            <div class="post post-small post-list education-post post-separator-border">
                <div class="post-wrap">
                    <!-- Image -->
                    <a class="image" href="{{$n->link()}}">
                        <img width="300" height="169" src="{{asset('public/images/'.$n->image)}}" class="attachment-medium size-medium wp-post-image" alt="" sizes="(max-width: 300px) 100vw, 300px">
                    </a>
                    <!-- Content -->
                    <div class="content">
                        <!-- Title -->
                        <h5 class="title"> 
                         <a href="{{$n->link()}}">{{$n->title}}
                         </a>
                        </h5>
                    </div>
                </div>
            </div><!-- Small Post End -->
            @endforeach
    
                        
    
                </div>
               <div class="tab-pane fade " id="tabpopularnews" aria-expanded="false">
                    

                    <div id="widget_tptn_pop-3" class="single-footer widget tptn_posts_list_widget"><h4 class="widget-title">Popular posts:</h4><div class="tptn_posts  tptn_posts_widget tptn_posts_widget3"><ul>

                        @foreach($popular as $n)
           
                                <li><a href="{{$n->link()}}" class="tptn_link"><img src="{{asset('public/images/'.$n->image)}}" alt="How to save money by cutting the cable" title="How to save money by cutting the cable" width="124" height="82" class="tptn_thumb tptn_featured"></a><span class="tptn_after_thumb"><a href="{{$n->link()}}" class="tptn_link"><span class="tptn_title">{{$n->title}}</span></a></span></li>

                              @endforeach


                    </ul><div class="tptn_clear"></div></div></div>                                         
               
                    
                </div>
                            </div>

        </div>
        </div>
    </div>
</div></div>
        </div></div></div></div></div></section>

   <section class="kc-elm kc-css-56424 kc_row video_news_area mb-50 no_space"><div class="kc-row-container  kc-container"><div class="kc-wrap-columns"><div class="kc-elm kc-css-550499 kc_col-sm-8 kc_column kc_col-sm-8"><div class="kc-col-container">  
    <div class="kc-elm kc-css-718406 ">
        
        
            
          @foreach($breakingNew as $n)
                            <!-- Post Start -->
                <div class="post post-small post-list post-dark popular-post">
                    <div class="post-wrap">
                        <a class="image" href="{{$n->link()}}"><img width="125" height="151" src="{{asset('public/images/'.$n->image)}}" class="attachment-hashnews_size_125x151 size-hashnews_size_125x151 wp-post-image" alt=""></a>
                        <div class="content fix">
                            <h5 class="title"><a href="{{$n->link()}}">{{$n->title}}</a></h5>
                            <p>{!! str_limit($n->details,200) !!}</p>
                            <a href="{{$n->link()}}" class="read-more">continue reading</a>
                        </div>
                    </div>
                </div><!-- Post Start -->

                @endforeach
                    

    </div>
    </div></div><div class="kc-elm kc-css-894158 kc_col-sm-4 kc_column kc_col-sm-4"><div class="kc-col-container">          <div class="kc-elm kc-css-841050 col-lg-12 col-md-6 col-12 pad_zero home_categories">

                <div class="sidebar-block-wrapper">
                                            <div class="head video-head">
                            <h4 class="title">Hot Categories</h4>
                        </div>
                                        <div class="body">
                        <ul class="sidebar-category video-category">
                        @foreach($catMenu as $cat)
              <li id="menu-item-111336" class="cat-item cat-item-{{$cat->id}}"><a href="{{url('/v/'.$cat->id.'/'.$cat->slug)}}">{{$cat->name}}</a>
                  <ul class="children">
                   @foreach($cat->children as $child)
                      <li id="menu-item-{{$child->id}}" class="cat-item cat-item-{{$child->id}}">
                        <a href="{{url('/v/'.$child->id.'/'.$child->slug)}}">{{$child->name}}</a></li>
                   @endforeach

                  </ul>
              </li>
              @endforeach
                        </ul>
                    </div>
                </div>

            </div>
        <div class="widget widget_hashnews_newsletter_widget kc-elm kc-css-264031">                 <div class="sidebar-subscribe">
                        <h4>Subscribe Now To <br>Our eNewsLetter:</h4>
<p>Plus receive discounts from our partners!</p>
<form action="{{route('subscribe')}}" method="post" class="mc4wp-form mc4wp-form-47" >
            {{csrf_field()}}
                        <div class="mc4wp-form-fields">
                            <input type="email" name="email" placeholder="Your email address" required="">
<input type="submit" value="Sign up">
</div><label style="display: none !important;">Leave this field empty if you're human: <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off"></label><input type="hidden" name="_mc4wp_timestamp" value="1553346375"><input type="hidden" name="_mc4wp_form_id" value="47"><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1"><div class="mc4wp-response"></div></form><!-- / MailChimp for WordPress Plugin -->                 </div>
                </div><div class="widget widget_execphp kc-elm kc-css-45460">           <div class="execphpwidget"><a href="https://www.hhbathandsafety.com/in-home-consultations-lp2/" target="_blank"><img src="http://seniornews.com/wp-content/uploads/2019/02/Senior_300x250_01.png"></a></div>
        </div></div></div></div></div></section>



        <section class="kc-elm kc-css-397005 kc_row mb-50 no_space"><div class="kc-row-container  kc-container"><div class="kc-wrap-columns"><div class="kc-elm kc-css-90991 kc_col-sm-6 kc_column kc_col-sm-6"><div class="kc-col-container">  
    <div class="kc-elm kc-css-316626 ">
        
        <div class="post-block-wrapper">

            <div class="head"><h4 class="title">All</h4></div>          <div class="body ">
                <div class="slick-row-active post-block-carousel gadgets-post-carousel" data-slick-items="1" data-slick-rows="1" data-slick-arrows="true" data-slick-autoplay="false" data-slick-autoplayspeed="5000" data-slick-loop="false" data-slick-desktop="1" data-slick-tablate="1">                                
                     @foreach($all as $n)
                      <div class="post gadgets-post">
                     <div class="post gadgets-post">
                        <div class="post-wrap">
                            <a class="image" href="{{$n->link()}}" tabindex="0"><img width="370" height="168" src="{{asset('public/images/'.$n->image)}}" class="attachment-hashnews_size_370x168 size-hashnews_size_370x168 wp-post-image" alt=""></a>
                            <div class="content">
                                <h4 class="title"><a href="{{$n->link()}}" class="tptn_link"><span class="tptn_title">{{$n->title}}</span></a></h4>
                                                            </div>
                        </div>
                         </div>
                          </div>
                        @endforeach




                </div>          </div>

        
        </div>

    </div>
    </div></div><div class="kc-elm kc-css-475886 kc_col-sm-6 kc_column kc_col-sm-6"><div class="kc-col-container">  
    <div class="kc-elm kc-css-696555 ">
        
        <div class="post-block-wrapper">

            <div class="head"><h4 class="title">Other News</h4></div>           <div class="body ">
                <div class="slick-row-active post-block-carousel gadgets-post-carousel" data-slick-items="1" data-slick-rows="1" data-slick-arrows="true" data-slick-autoplay="false" data-slick-autoplayspeed="5000" data-slick-loop="false" data-slick-desktop="1" data-slick-tablate="1">                                  
                      @foreach($other as $n)
                       <div class="post gadgets-post">
                        <div class="post-wrap">
                            <a class="image" href="{{$n->link()}}" tabindex="0"><img width="370" height="168" src="{{asset('public/images/'.$n->image)}}" class="attachment-hashnews_size_370x168 size-hashnews_size_370x168 wp-post-image" alt=""></a>
                            <div class="content">
                                <h4 class="title"><a href="{{$n->link()}}" class="tptn_link"><span class="tptn_title">{{$n->title}}</span></a></h4>
                                                            </div>
                        </div>
                         </div>
                        @endforeach

                                   

                </div>          </div>

        
        </div>

    </div>
    </div></div></div></div></section><div class="tptn_counter" id="tptn_counter_64"></div><div class="swp-content-locator"></div>  </div><!-- #primary -->

    </div><!-- #content -->
@stop
