   <div class="col-lg-4 col-12 mb-50">
        
<aside id="secondary" class="widget-area mb-50">
  <div class="row">
     

    <div id="text-2" class="single-sidebar col-lg-12 col-md-6 col-12 widget_text">
      <div class="sidebar-block-wrapper">
      <div class="head feature-head"><h4 class="title">Our Partner Offers</h4></div>     <div class="textwidget">
         @foreach($Addvertise as $v)
          <a href="{{url('/click-add/'.$v->id)}}" target="_blank"> 
              @if($v->advert_type == 1)
                <img  src="{{asset('public/images/'.$v->val1)}}" alt="" />
              @else
                {!! $v->val !!}
              @endif
            </a>
          @endforeach
      
        </div>
    </div></div>


    <div id="jiv-author-2" class="single-sidebar col-lg-12 col-md-6 col-12 jiv-author"><div class="sidebar-block-wrapper"><div class="head feature-head"><h4 class="title">Featured Author</h4></div>            <div class="widget-author-container p-3">
                <div class="widget-author">
                    <div class="row">
                        <div class="col-sm-4">
                                            <a href="{{$featuredAuthor->link()}}"><img class="rounded-circle" src="{{$featuredAuthor->get_gravatar()}}" alt="" width="100" height="100">
                         
                        </a>
                                        </div>
                <div class="col-sm-8 p-3">
                    
                    
                        <h4 class="mb-0">{{$featuredAuthor->first_name}} {{$featuredAuthor->last_name}}</h4> 
                        
                                                    
                            <span style="font-size: 14px;">{{$featuredAuthor->job}}</span>
                                                
                                      
                    </div>
                    <div class="col-sm-12 mt-2">
                        <p>{{$featuredAuthor->info}}</p>
                    </div>
                     </div>                 
                    
                    <a href="{{url('author/all')}}" class="btn btn-primary mt-2" style="background: #00c8fa;border: #00c8fa;">View All Authors</a>
                </div>
            </div>
            </div></div><div id="categories-2" class="single-sidebar col-lg-12 col-md-6 col-12 widget_categories"><div class="sidebar-block-wrapper"><div class="head feature-head"><h4 class="title">Categories</h4></div>  
             <ul>
          @foreach($catMenu as $cat)
              <li id="menu-item-{{$cat->id}}" class="cat-item cat-item-{{$cat->id}}">
              <a href="{{url('/v/'.$cat->id.'/'.$cat->slug)}}">{{$cat->name}}</a>
              </li>
                @foreach($cat->children as $child)
                   <li id="menu-item-{{$child->id}}" class="cat-item cat-item-{{$child->id}}">
                    <a href="{{url('/v/'.$child->id.'/'.$child->slug)}}">{{$child->name}}</a>
                 </li>
                @endforeach
              @endforeach  
    </ul>
</div></div>    <div id="recent-posts-2" class="single-sidebar col-lg-12 col-md-6 col-12 widget_recent_entries"><div class="sidebar-block-wrapper">   <div class="head feature-head"><h4 class="title">Recent Posts</h4></div>   
 <ul>
            @foreach($popular as $n)
          <li>
              <a href="{{$n->link()}}">{{$n->title}}</a>
           </li>
           @endforeach
          
          </ul>
    </div></div><div id="recent-comments-2" class="single-sidebar col-lg-12 col-md-6 col-12 widget_recent_comments"><div class="sidebar-block-wrapper"><div class="head feature-head"><h4 class="title">Recent Comments</h4></div>
    <ul id="recentcomments">
       @foreach($recentComments as $comment)
      <li class="recentcomments"><span class="comment-author-link">
        
        <a href="{{url('/article/'.$comment->news->id.'/'.$comment->news->slug)}} " rel="external nofollow" class="url">{{$comment->comment}}</a></span> on <a href="{{url('/article/'.$comment->news->id.'/'.$comment->news->slug)}} ">{{$comment->news->title}}</a></li>
         @endforeach
      </ul></div></div> </div>
</aside><!-- #secondary -->     </div>