@extends('welcome5')

@section('cat_feature')

<div id="content" class="site-content">
      <div class="page-wrapper blog-story-area mt-4">
  <div class="container">
    <div class="row">

            <!-- single blog right sidebar start -->
   <div class="col-md-8">
          <div class="kc_clfw"></div><section class="kc-elm kc-css-821726 kc_row"><div class="kc-row-container  kc-container"><div class="kc-wrap-columns"><div class="kc-elm kc-css-662433 kc_col-sm-12 kc_column kc_col-sm-12"><div class="kc-col-container"><div class="widget gform_widget kc-elm kc-css-321582"><h2 class="widgettitle">Guest Writers &amp; Bloggers</h2><link rel="stylesheet" id="gforms_reset_css-css" href="http://seniornews.com/wp-content/plugins/gravityforms/css/formreset.min.css?ver=2.3.4.3" type="text/css" media="all">
<link rel="stylesheet" id="gforms_formsmain_css-css" href="http://seniornews.com/wp-content/plugins/gravityforms/css/formsmain.min.css?ver=2.3.4.3" type="text/css" media="all">
<link rel="stylesheet" id="gforms_ready_class_css-css" href="http://seniornews.com/wp-content/plugins/gravityforms/css/readyclass.min.css?ver=2.3.4.3" type="text/css" media="all">
<link rel="stylesheet" id="gforms_browsers_css-css" href="http://seniornews.com/wp-content/plugins/gravityforms/css/browsers.min.css?ver=2.3.4.3" type="text/css" media="all">
<script type="text/javascript" src="http://seniornews.com/wp-content/plugins/gravityforms/js/jquery.maskedinput.min.js?ver=2.3.4.3"></script>

                <div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_2">

           <form id="gform_2"  class="form-horizontal" enctype="multipart/form-data" name="staffrm" action="{{url('/author/add')}}" method="post">
                {{csrf_field()}}
                        <div class="gform_body"><ul id="gform_fields_2" class="gform_fields top_label form_sublabel_below description_below"><li id="field_2_2" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label gfield_label_before_complex" for="input_2_2_3">Name<span class="gfield_required">*</span></label><div class="ginput_complex ginput_container no_prefix has_first_name no_middle_name has_last_name no_suffix gf_name_has_2 ginput_container_name gfield_trigger_change" id="input_2_2">
                            
                            <span id="input_2_2_3_container" class="name_first">
                          <input type="text" name="first_name" id="input_2_2_3" value="" aria-label="First name" tabindex="2" aria-required="true" aria-invalid="false">
                                                    <label for="input_2_2_3">First</label>
                                                </span>
                            
                            <span id="input_2_2_6_container" class="name_last">
                                                    <input type="text" name="last_name" id="input_2_2_6" value="" aria-label="Last name" tabindex="4" aria-required="true" aria-invalid="false">
                                                    <label for="input_2_2_6">Last</label>
                                                </span>
                            
                        </div>
                        <div class="gfield_description"><br></div>
                      </li>
                        <li id="field_2_18" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_2_18">Email<span class="gfield_required">*</span></label>
                          <div class="ginput_container ginput_container_email">
                            <input name="email" id="input_2_18" type="text" value="" class="medium" tabindex="6" aria-required="true" aria-invalid="false">
                        </div>
                        <div class="gfield_description"><br></div></li>

                        <li id="field_2_18" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_2_18">Password<span class="gfield_required">*</span></label>
                          <div class="ginput_container ginput_container_email">
                            <input name="password" id="input_2_18" type="text" value="" class="medium" tabindex="6" aria-required="true" aria-invalid="false">
                        </div>
                        <div class="gfield_description"><br></div></li>

                        <li id="field_2_18" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_2_18">Re Password
                          <span class="gfield_required">*</span></label>
                          <div class="ginput_container ginput_container_email">
                            <input name="password_confirmation" id="input_2_18" type="text" value="" class="medium" tabindex="6" aria-required="true" aria-invalid="false">
                        </div>
                        <div class="gfield_description"><br></div></li>


                        <li id="field_2_4" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_2_4">Phone<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_phone">
                          <input name="phone" id="input_2_4" type="text" value="" class="medium" tabindex="7" aria-required="true" aria-invalid="false"></div><div class="gfield_description"><br></div></li><li id="field_2_5" class="gfield field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_2_5">Your Website or Blog</label><div class="ginput_container ginput_container_website">
                    <input name="input_5" id="input_2_5" type="text" value="" class="medium" tabindex="8" aria-invalid="false">
                </div><div class="gfield_description"><br></div></li><li id="field_2_13" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_2_13">Info on the Author:<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_textarea"><textarea name="input_13" id="input_2_13" class="textarea medium" tabindex="9" aria-required="true" aria-invalid="false" rows="10" cols="50"></textarea></div><div class="gfield_description"><br></div></li><li id="field_2_16" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_2_16">Attach Sample Article:<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_textarea"><textarea name="input_16" id="input_2_16" class="textarea medium" tabindex="10" aria-required="true" aria-invalid="false" rows="10" cols="50"></textarea></div><div class="gfield_description"><br></div></li><li id="field_2_17" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_2_17">How did you hear about Seniornews.com?<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_select"><select name="input_17" id="input_2_17" class="medium gfield_select" tabindex="11" aria-required="true" aria-invalid="false"><option value="Please Select">Please Select</option><option value="Member">Member</option><option value="Email">Email</option><option value="Internet Search">Internet Search</option><option value="Facebook">Facebook</option><option value="Twitter">Twitter</option><option value="Friend">Friend</option><option value="Other">Other</option></select></div><div class="gfield_description"><br></div></li>
                            </ul></div>
        <div class="gform_footer top_label"> <input type="submit" id="gform_submit_button_2" class="gform_button button" value="Submit" tabindex="12" onclick="if(window[&quot;gf_submitting_2&quot;]){return false;}  window[&quot;gf_submitting_2&quot;]=true;  " onkeypress="if( event.keyCode == 13 ){ if(window[&quot;gf_submitting_2&quot;]){return false;} window[&quot;gf_submitting_2&quot;]=true;  jQuery(&quot;#gform_2&quot;).trigger(&quot;submit&quot;,[true]); }"> 
            <input type="hidden" class="gform_hidden" name="is_submit_2" value="1">
            <input type="hidden" class="gform_hidden" name="gform_submit" value="2">
            
            <input type="hidden" class="gform_hidden" name="gform_unique_id" value="">
            <input type="hidden" class="gform_hidden" name="state_2" value="WyJbXSIsIjZiZDdiY2VjMTk5MWIzMzQ0YTZjZTI4NjcyMWZkOGQyIl0=">
            <input type="hidden" class="gform_hidden" name="gform_target_page_number_2" id="gform_target_page_number_2" value="0">
            <input type="hidden" class="gform_hidden" name="gform_source_page_number_2" id="gform_source_page_number_2" value="1">
            <input type="hidden" name="gform_field_values" value="">
            
        </div>
                        </form>
                        </div><script type="text/javascript"> jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 2) {jQuery('#input_2_4').mask('(999) 999-9999').bind('keypress', function(e){if(e.which == 13){jQuery(this).blur();} } );} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );</script><script type="text/javascript"> jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [2, 1]) } ); </script></div></div></div></div></div></section><div class="tptn_counter" id="tptn_counter_111805"></div><div class="swp-content-locator"></div>
      </div>
      
      @include('home5.content_left')

      <!--single blog right sidebar end -->
      
    </div>
  </div>
</div>

  </div>



@stop


