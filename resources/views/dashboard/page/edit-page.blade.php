@extends('dashboard')

@section('sub-title')
| News
@stop


@section('content-title')
	Edit News

	<a href="{{route('pages')}}" type="button" class="pull-right btn  btn-primary btn-flat"><i class="glyphicon glyphicon-arrow-left"></i> <b>Back</b> </a>
@stop
@section('style')
<link rel="stylesheet" href="{{asset('public/assets/admin/bootstrap/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

@stop
@section('content')

<div class="row">
    <div class="col-xs-10 col-xs-offset-1">
    		
       <div class="box box-info">
            <div class="box-header with-border">
                <h5>Last Edited:   <b style="color: red">
                @if($page->created_at) {{$page->updated_at->toDayDateTimeString()}} @endif</b></h5>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" name="editNewsForm" action="{{route('updatepage')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$page->id}}">
            
              <div class="box-body">
               
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Slide Title *</label>
                  <div class="col-sm-8">
                    <input type="text" name="title" class="form-control" value="{{$page->title}}" required autofocus>
                  </div>
                </div>

                <br>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">News Details *</label>
                  <div class="col-sm-8">  
                        <textarea class="textarea" name="details" style="width: 100%; min-height: 420px;" rows="20" >{{$page->details}}</textarea>
                  </div>
            
            </div>
        
        </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-success col-sm-8 col-sm-offset-3">Update New Slide</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
         
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@stop
@section('script')

<script src="{{asset('public/assets/admin/js/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{ asset('public/assets/admin/js/bootstrap-toggle.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/js/bootstrap-fileinput.js') }}"></script>
<script>
  $(function () {
    $(".textarea").wysihtml5();
  });
</script>

<script>
$(document).ready(function () {
    $('#bootstrapTagsInputForm')
        .find('[name="source"]')
            // Revalidate the cities field when it is changed
            .change(function (e) {
                $('#bootstrapTagsInputForm').formValidation('revalidateField', 'source');
            })
            .end()
        
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                source: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter at least one tag you like the most.'
                        }
                    }
                }
            }
        });
});
</script>
@stop