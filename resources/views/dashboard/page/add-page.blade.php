@extends('dashboard')

@section('sub-title')
| Pages
@stop


@section('content-title')
	Add New Page

	<a href="{{route('pages')}}" type="button" class="pull-right btn  btn-primary btn-flat"><i class="glyphicon glyphicon-arrow-left"></i> <b>Back</b> </a>
@stop

@section('style')
<link rel="stylesheet" href="{{asset('public/assets/admin/bootstrap/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

@stop

@section('content')

<div class="row">
    <div class="col-xs-10 col-xs-offset-1">
    		
       <div class="box box-info">
            <div class="box-header with-border">
              
                @include('errors.errors')
            </div>
            <form class="form-horizontal"  method="post" action="{{route('savepage')}}" enctype="multipart/form-data">
            {{csrf_field()}}
              <div class="box-body">

                <div class="form-group">
                  <label class="col-sm-3 control-label">Page Title *</label>
                  <div class="col-sm-8">
                    <input type="text" name="title" class="form-control"/>
                  </div>
                </div>
                

               
                <div class="form-group">
                  <label class="col-sm-3 control-label"> Details *</label>
                  <div class="col-sm-8">
                        <textarea rows=20 class="textarea" name="details" id="area2" style=" width: 100%; min-height: 320px;"></textarea>
                  </div>
            </div>

            
              <div class="box-footer">
                <button type="submit" class="btn btn-success col-sm-8 col-sm-offset-3">Add New Page</button>
              </div>
            </form>
          </div>
         
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
@stop


@section('script')

<script src="{{asset('public/assets/admin/js/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{ asset('public/assets/admin/js/bootstrap-toggle.min.js') }}"></script>
<script src="{{ asset('public/assets/admin/js/bootstrap-fileinput.js') }}"></script>

<script>
  $(function () {
    $(".textarea").wysihtml5();
  });
</script>


<script>
$(document).ready(function () {
    $('#bootstrapTagsInputForm')
        .find('[name="source"]')
            .change(function (e) {
                $('#bootstrapTagsInputForm').formValidation('revalidateField', 'source');
            })
            .end()
        
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                source: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter at least one tag you like the most.'
                        }
                    }
                }
            }
        });
});
</script>
@stop