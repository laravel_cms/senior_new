<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    
    protected $guarded = [''];
    public $level;
    public function posts() {
        return $this->hasMany('App\News')->where('status',1)->latest();
    }
   
  /*  public function children() {
	    return $this->hasMany(App\Category::class, 'parent_id', 'id');
	 }*/

	/*  public function getChildren() {
        return $this->hasMany('App\Category')->latest();
	 }
*/
	  public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }
    public function childrenRecursive()
	{
	   return $this->children()->with('childrenRecursive');
	}

	 static function recursiveArray()
	{
	    $rs = array();
        $categories = self::where('parent_id',0)->get();
        foreach ($categories as $key => $cate) {
            $cate->level =1;
            self::recursive($cate,$rs );
        }
        return $rs;
	}

	 static function recursive($parent ,&$rs ){
	 	$str ="";
	 	for ($i=0; $i < $parent->level; $i++) { 
	 		$str .="----";
	 	}
	 	
	 	$parent->name = $str.$parent->name ;
        $rs[]= $parent;
        foreach ($parent->children as $key => $child) {
            $child->level = $parent->level +1;
           self::recursive($child,$rs);
        }
    }
}
