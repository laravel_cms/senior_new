<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Gsetting;
use App\Social;
use App\News;
use App\Category;
use App\Video;
use App\Seo;
use App\Comment;
use App\User;

use App\Advertisement;
use Carbon\Carbon;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
       /**
        * Advetisement Code
        *
        */
        $Addvertise = Advertisement::where('status',1)->where('advert_size' , 5)->orderByRaw('RAND()')->take(1)->get();
            View::share('Addvertise', $Addvertise);
        $AddTop = Advertisement::where('status',1)->where('advert_size' , 3)->orderByRaw('RAND()')->take(1)->get();
            View::share('AddTop', $AddTop);
        $Addleft = Advertisement::where('status',1)->where('advert_size' , 4)->orderByRaw('RAND()')->take(1)->get();
            View::share('Addleft', $Addleft);
        $Addright = Advertisement::where('status',1)->where('advert_size' , 1)->orderByRaw('RAND()')->take(1)->get();
            View::share('Addright', $Addright);
        $Addright2 = Advertisement::where('status',1)->where('advert_size' , 2)->orderByRaw('RAND()')->take(1)->get();
            View::share('Addright2', $Addright2);

        $catMenu = /*Category::with('childrenRecursive')->where('parent_id',0)->where('status',1)->orderby('position','asc')->get();*/
                    Category::with('childrenRecursive')->where('parent_id',0)->where('status',1)->orderby('position','asc')->get();
    /*  foreach ($catMenu as $key => $cat) {
                foreach ($cat->childrenRecursive as $key => $child) {
                    print($child);
                 }   
          }   die;    */ 
        View::share('catMenu', $catMenu);

        // $videoMenu = Video::orderBy(DB::raw('RAND()'))->latest()->where('status',1)->paginate(3);
        $videoMenu = Video::latest()->where('status',1)->get();
        View::share('videoMenu',$videoMenu);
        
        $Gsetting = Gsetting::findOrFail(1);
        View::share('Gsetting', $Gsetting);
        View::share('title', $Gsetting->title);



        /*News for breaking*/
        $news = News::latest()->limit(10)->where('status',1)->where('breaking',1)->where('updated_at', '=', Carbon::today())->get();
        View::share('news', $news);


        $popular = News::with('category')->limit(4)->where('status',1)
                    ->orderby('hit_count','desc')->get();
        View::share('popular', $popular);

        $lastest = News::limit(4)->where('status',1)
                    ->orderby('updated_at','desc')->get();
        View::share('lastest', $lastest);
          
        $gallery = News::with('category')->latest()->where('status',1)->paginate(9);
        View::share('gallery', $gallery);

        $newsForSeo = News::where('status',1)->get();
        View::share('newsForSeo', $newsForSeo);

        $Social = Social::where('status',1)->get();
        View::share('Social', $Social);

         $recentComments = Comment::with('news')->limit(5)->orderby('updated_at','desc')->get();
        View::share('recentComments', $recentComments);
       // echo $recentComments->count();die;
        /*foreach ($recentComments as $key => $c) {
             var_dump($c->news );die;
        }*/
      

        $seoComment = Seo::findOrFail(1);
        View::share('seoComment', $seoComment);
        View::share('meta_keyword', $seoComment->metakeyword);
        View::share('meta_description', $seoComment->metakeyword);

        $featuredAuthor = User::
            join('role_users', 'users.id', '=', 'role_users.user_id')
            ->where('role_users.role_id', 3)
            ->select('users.*')
            ->inRandomOrder()
            ->first();
        View::share('featuredAuthor', $featuredAuthor);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
