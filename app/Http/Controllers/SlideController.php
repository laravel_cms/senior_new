<?php

namespace App\Http\Controllers;

use App\Slide;
use App\Category;
use Illuminate\Http\Request;
use DB;
use Session;
use Carbon\Carbon;
use File;
use Intervention\Image\Facades\Image;
use Input;
use Sentinel;
class SlideController extends Controller
{
  public function __construct()
    {
        $this->middleware('role:admin-manager');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $menu = view('dashboard.menubar');
        $category = Category::where('status', 1)->get();
        $allSlides = Slide::orderBy('created_at', 'desc')->get();
        $content = view('dashboard.slides.slides',compact('allSlides','category'));
        return view('dashboard', compact('menu','content'));
    }
    public function addslides()
    {
        $menu = view('dashboard.menubar');
        $category = Category::where('status', 1)->get();
        $content = view('dashboard.slides.add-slides', compact('category'));
        return view('dashboard', compact('menu','content'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
                'title' => 'required',
                'category_id' => 'required',
            ]);
         $data = array();
       
        $data['title'] = $request->title;
        $data['link'] = $request->link;
        $data['category_id'] =   $request->category_id;        
        $data['type'] =   $request->type;        
        $data['user_id'] = Sentinel::getUser()->id;
        $data['created_at'] = Carbon::now(); 
        $data['updated_at'] = Carbon::now(); 

        
        if($request->hasFile('image')){
            $image = $request->file('image');

            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;

            $location = public_path('images'). '/' . $image_full_name;
            if( $data['type'] == 1){
                Image::make($image)->resize(585,445)->save($location);
            }else{
                Image::make($image)->resize(290,222)->save($location);
            }
            $data['image'] = $image_full_name;

            Slide::create($data);
            alert()->success('Good Job', 'Successfully added a post !!');
                return redirect('/slides');
        }else{
            Slide::create($data);
            alert()->success('Good Job', 'Successfully added a post !!');
                return redirect('/slides');
        }
    }


    public function editslides(Request $request, $id)
    {
        $slides = Slide::findOrFail($id);
        $category = Category::where('status', 1)->get();
        $menu = view('dashboard.menubar');
        $content = view('dashboard.slides.edit-slides',compact('slides','category'));
        return view('dashboard', compact('menu','content'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slide  $slides
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
       
        $data['title'] = $request->title;
        $data['link'] = str_slug($request->link);
        $data['type'] = str_slug($request->type);
        
        $data['category_id'] = $request->category_id;        
        $image = $request->file('image');
       
     
        $data['updated_at'] = Carbon::now(); 
        $data['user_id'] = Sentinel::getUser()->id;

          
           // Upload the new picture
           
        
        if($request->hasFile('image')){
            $image = $request->file('image');
                  // Delete the old picture
            $update = Slide::find($id);

            $update_path = public_path('images'). '/' .$update->image;
            if($update->image != null)
            {
                @unlink($update_path);
            }

            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;

            $location = public_path('images'). '/' . $image_full_name;
            Image::make($image)->resize(540,400)->save($location);
            $data['image'] = $image_full_name;

            $update->update($data);
         
                alert()->success('Good Job', 'Successfully Updated !!');
                return redirect('/slides');
            }
        else {
            DB::table('slides')->where('id', $id)->update($data);
            alert()->success('Good Job', 'Successfully Updated !!');
            return redirect('/slides');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slide  $slides
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $slides = Slide::find($id);

        $path = public_path('images'). '/' .$slides->image;
         if($slides->image == null)
            {
                $slides->delete();
            }else{
                unlink($path);
            }

        $slides->delete();
        alert()->success('Good Job', 'Successfully Deleted !!');
        return back();
    }


    public function publish($id)
    {
        Slide::where('id', $id)->update(['status' => 1]); 
        alert()->success('Good Job', 'Successfully Published !!');
        return back();
    }
    public function unpublish($id)
    {
        Slide::where('id', $id)->update(['status' => 0]); 
        alert()->success('Good Job', 'Successfully Unpublished !!');
        return back();
    }
}
