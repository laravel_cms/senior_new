<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Category;
use App\News;
use App\Video;
use App\Gsetting;
use App\Advertisement;
use Carbon\Carbon;
use App\AllImage;
use App\Theme;
use App\User;
use Sentinel;
use Illuminate\Support\Facades\Input;
use Purifier;
use App\Comment;
use Illuminate\Support\Facades\View;
use App\Slide;

class HomeController extends Controller
{
    public function index()
    {
       /** 
        * Start Slider Code
        */
       $featuredCateArr = array();
        $featuredCates = Category::where('feature',1)->where('status',1)->orderby('position','asc')->get();
        foreach ($featuredCates as $key => $cate) {
           $x = array('name'=>$cate->name,'cate_id'=>$cate->id);
           $posts =  News::with('user')->orderBy('updated_at','asc')->where('status',1)->where('category_id',$cate->id)
                        ->take(4)->get();
                       // print_r($posts);die;
            $i=0;
             $x['list_post'] = array();
            foreach ($posts as $key => $post) {
               /* echo "<pre>";
        print_r($post->user);
        echo "</pre>";die;*/
                if($i==0){
                   $x['first_post']= $post;
                }else{
                    $x['list_post'][]= $post;
                }
                $i++;
            }             
           $featuredCateArr[] = $x;          
        }

        $home1Cate = Category::where('home1',1)->where('status',1)->orderby('position','asc')->first();
        $home1CateArr = array('name'=>$home1Cate->name,'cate_id'=>$home1Cate->id);
        $home1CateArr['list_post'] =  News::orderBy('updated_at','asc')->where('status',1)->where('category_id',$home1Cate->id)->take(8)->get();
        
        $home2Cate = Category::where('home2',1)->where('status',1)->orderby('position','asc')->first();
        $home2CateArr = array('name'=>$home2Cate->name,'cate_id'=>$home2Cate->id);
        $home2CateArr['list_post'] = News::orderBy('updated_at','asc')->where('status',1)->where('category_id',$home2Cate->id)->take(4)->get();
        $all =  News::orderBy('updated_at','desc')->where('status',1)->where('all',1)->take(4)->get();
        $other =  News::orderBy('updated_at','desc')->where('status',1)->where('other',1)->take(4)->get();

       /* echo "<pre>";
        print_r($featuredCateArr);
        echo "</pre>";die;*/
        $c = News::orderBy('updated_at','asc')->where('status',1)->where('slider',1)->count();
        $tk= 1; $skp = $c-$tk; $last = 2; $skp2 = $skp-$last; $one =2; $skp3 = $skp2-$one;
            
        $slider = Slide::with('category')->where('type',1)->orderBy('updated_at','asc')->get();
        $slider2 = Slide::with('category')->where('type',2)->orderBy('updated_at','asc')->take(2)->skip(0)->get();
        $slider3 = Slide::with('category')->where('type',3)->orderBy('updated_at','asc')->take(2)->skip(0)->get();
        
        $breakingNew =  News::orderBy('updated_at','asc')->where('status',1)->where('breaking',1)
                        ->take(6)->get();              

       /** 
        * End Slider Code
        */

       /** 
        * Start  Latest news Code
        */
        // $news = News::latest()->where('status',1)->where('featured',1)->where('updated_at', '>=', Carbon::today())->get();
        $news = News::with('category')->latest()->where('status',1)->where('featured',1)->get();
        
        /* Start For Category with news post Code in bootom section */
        $count = Category::count(); $take = 3; $skip =  $count - $take;
        $catNews = Category::with('posts')->orderBy('position','desc')->where('status',1)
                ->skip($skip)->take($take)->get();
        
        /* Start For Category with news post Code in left content section */
        $catN = 3; $lastSkip = $skip - $catN;
        $catNews2 = Category::with('posts')->orderBy('position','desc')
                    ->where('status',1)->take($catN)->skip($lastSkip)->get();
        /* Start For Video section*/
        $video = Video::all();
        
        $theme = Theme::first();
        

        if($theme->status == 1) {
            $data['newsSlider'] = view('home.slider',compact('slider','slider2','slider3'));
            $data['latestNews'] = view('home.latestNews',compact('news'));
            $data['follow-us'] = view('home.social');
            $data['popular'] = view('home.popular');
            $data['categoryNews'] = view('home.featur_catNews',compact('catNews'));    
            $data['lifeStyle'] = view('home.lifeStyle',compact('catNews2'));
            $data['video'] = view('home.video',compact('video'));
            $data['subscribe'] = view('home.subscribe');
            $data['addvert'] = view('home.addvert');
            return view('welcome', $data);   
        }elseif ($theme->status == 2) {
            $data2['follow-us'] = view('home2.social');
            $data2['addvert'] = view('home2.addvert');
            $data2['newsSlider'] = view('home2.slider',compact('slider','slider3'));
            $data2['popular'] = view('home2.popular');
            $data2['subscribe'] = view('home2.subscribe');
            $data2['lifeStyle'] = view('home2.lifeStyle',compact('catNews2'));
            $data2['latestNews'] = view('home2.latestNews',compact('news'));
            $data2['categoryNews'] = view('home2.featur_catNews',compact('catNews')); 
            return view('welcome2',$data2);   
        }elseif ($theme->status == 3) {
            $view3['newsSlider'] = view('home3.slider',compact('slider','slider2','slider3'));
            $view3['follow-us'] = view('home3.social');
            $view3['lifeStyle'] = view('home3.lifeStyle',compact('catNews2'));
            $view3['categoryNews'] = view('home3.featur_catNews',compact('catNews'));
            $view3['addvert'] = view('home3.addvert');
            $view3['popular'] = view('home3.popular');
            $view3['subscribe'] = view('home3.subscribe');
            return view('welcome3',$view3);   
        }elseif ($theme->status == 4) {
            $view4['categoryNews'] = view('home4.featur_catNews',compact('catNews')); 
            $view4['newsSlider'] = view('home4.slider',compact('slider','slider2','slider3'));
            $view4['lifeStyle'] = view('home4.lifeStyle',compact('catNews2'));
            $view4['follow-us'] = view('home4.social');
            $view4['popular'] = view('home4.popular');
            $view4['subscribe'] = view('home4.subscribe');
            $view4['addvert'] = view('home4.addvert');
            return view('welcome4',$view4);   
        }elseif ($theme->status == 5) {
                      /*  $view5['catNews'] = view('home5.category',compact('news'));
*/
           /* $view5['details'] = view('home5.home');
          $view5['categoryNews'] = view('home5.featur_catNews',compact('catNews')); 
          $view5['newsSlider'] = view('home5.slider',compact('slider','slider2','slider3'));
           // $view4['lifeStyle'] = view('home5.lifeStyle',compact('catNews2'));
            $view5['follow-us'] = view('home5.social');
           // $view4['popular'] = view('home5.popular');
            $view5['subscribe'] = view('home5.subscribe');
            $view5['addvert'] = view('home5.addvert');*/
            return view('home5.home',compact('all','other','slider','slider2','slider3','breakingNew',
                'featuredCateArr','home1CateArr','home2CateArr'));   
        }
    }

    public function category($id, $slug){

        
     
       
        $theme = Theme::find(1);
        $news = News::with('user')->latest()->where('category_id', $id)->where('status',1)->get();
        $cat = Category::latest()->where('id', $id)->where('status',1)->first();
/*echo "<pre>";print_r($cat->name) ;echo "</pre>";die;
*/      /*  foreach ($news as $key => $new) {
        echo "<pre>";print_r($new->user) ;echo "</pre>";
                }        die;*/
        //echo "<pre>";print_r($news->children) ;echo "</pre>";
        if($theme->status == 1){
            $data['catNews'] = view('home.category',compact('news'));
            $data['addvert'] = view('home.addvert');
            $data['follow-us'] = view('home.social');
            $data['popular'] = view('home.popular'); 
            $data['subscribe'] = view('home.subscribe');
            return view('welcome', $data);
        }elseif ($theme->status == 2){
            $data2['catNews'] = view('home2.category',compact('news'));
            $data2['addvert'] = view('home2.addvert');
            $data2['follow-us'] = view('home2.social');
            $data2['popular'] = view('home2.popular'); 
            $data2['subscribe'] = view('home2.subscribe');
            return view('welcome2', $data2);
        }elseif ($theme->status == 3) {
            $data3['addvert'] = view('home3.addvert');
            $data3['follow-us'] = view('home3.social');
            $data3['popular'] = view('home3.popular'); 
            $data3['subscribe'] = view('home3.subscribe');
            $data3['catNews'] = view('home3.category',compact('news'));
            return view('welcome3',$data3);   
        }elseif ($theme->status == 4) {
            $view4['follow-us'] = view('home4.social');
            $view4['popular'] = view('home4.popular');
            $view4['follow-us2'] = view('home4.follow-us');
            $view4['subscribe'] = view('home4.subscribe');
            $view4['addvert'] = view('home4.addvert');
            $view4['catNews'] = view('home4.category',compact('news'));
            return view('welcome4',$view4);   
        }elseif ($theme->status == 5) {
            $view5['follow-us'] = view('home5.social');
            $view5['popular'] = view('home5.popular');
            $view5['follow-us2'] = view('home5.follow-us');
            $view5['subscribe'] = view('home5.subscribe');
            $view5['addvert'] = view('home5.addvert');
            $view5['catNews'] = view('home5.category',compact('news','cat'));
            return view('welcome5',$view5);   
        }


    }
    
    public function featuredArticle($id, $nslug){
        $theme = Theme::first();
        /*Start Hit Counter for popular post */
        $hitNews = News::findOrFail($id);
        $data = array();
        $data['hit_count'] = $hitNews->hit_count+1;
        News::where('id',$id)->update($data);
        /*Start Hit Counter for popular post */

        $news = News::with('category')->where('id',$id)->get();
        $latestNews = News::with('category')->latest()->where('status',1)->take(10)->get();

        if($theme->status == 1) {
            $view['details'] = view('home.details',compact('news'));
            $view['addvert'] = view('home.addvert');
            $view['follow-us'] = view('home.social');
            $view['popular'] = view('home.popular'); 
            // $view['subscribe'] = view('home.subscribe');
            return view('welcome',$view);
        }elseif ($theme->status == 2) {
            $data2['details'] = view('home2.details',compact('news','latestNews'));
            $data2['addvert'] = view('home2.addvert');
            $data2['follow-us'] = view('home2.social');
            $data2['popular'] = view('home2.popular'); 
            $data2['subscribe'] = view('home2.subscribe');
            return view('welcome2', $data2);
        }elseif ($theme->status == 3) {
            $data3['addvert'] = view('home3.addvert');
            $data3['follow-us'] = view('home3.social');
            $data3['popular'] = view('home3.popular'); 
            $data3['subscribe'] = view('home3.subscribe');
            $data3['tags'] = view('home3.tags',compact('news'));
            $data3['details'] = view('home3.details',compact('news','latestNews'));
            return view('welcome3', $data3);   
        }elseif ($theme->status == 4) {
            $view4['details'] = view('home4.details',compact('news','latestNews'));
            $view4['follow-us'] = view('home4.social');
            $view4['follow-us2'] = view('home4.follow-us');
            $view4['popular'] = view('home4.popular');
            $view4['subscribe'] = view('home4.subscribe');
            $view4['addvert'] = view('home4.addvert');
            return view('welcome4',$view4);   
        }

    }
    public function article($newsid, $slug){
         
        $theme = Theme::first();

        /*Start Hit Counter for popular post */
        $hitNews = News::findOrFail($newsid);
        $data = array();
        $data['hit_count'] = $hitNews->hit_count+1;
        News::where('id',$newsid)->update($data);
        /*Start Hit Counter for popular post */
        $new = News::with('category')->with('comments')->where('id',$newsid)->first();
        $title=$new->title;
      //  $comments = $new->comment;
   $comments =  $new->comments()->with('user')->get();
         /*  print_r($comments);
       die;*/
       /*foreach ($comments as $comment){
        echo "<pre>";print_r($comment->user->get_gravatar());  echo "</pre>"; 
       }die;*/
        $latestNews = News::with('category')->latest()->where('status',1)->take(10)->get();
        if($theme->status == 1) {
            $view['details'] = view('home.details',compact('new','comments'));
            $view['addvert'] = view('home.addvert');
            $view['follow-us'] = view('home.social');
            $view['popular'] = view('home.popular'); 
            // $view['subscribe'] = view('home.subscribe');
            return view('welcome',$view);
        }elseif ($theme->status == 2) {
            $view2['details'] = view('home2.details',compact('new','latestNews'));
            $view2['addvert'] = view('home2.addvert');
            $view2['follow-us'] = view('home2.social');
            $view2['popular'] = view('home2.popular'); 
            $view2['subscribe'] = view('home2.subscribe');
            return view('welcome2',$view2);
        }elseif ($theme->status == 3) {
            $view3['details'] = view('home3.details',compact('new','latestNews'));
            $view3['addvert'] = view('home3.addvert');
            $view3['follow-us'] = view('home3.social');
            $view3['popular'] = view('home3.popular'); 
            $view3['subscribe'] = view('home3.subscribe');
            return view('welcome3',$view3);   
        }elseif ($theme->status == 4) {
            $view4['details'] = view('home4.details',compact('new','latestNews'));
            $view4['follow-us'] = view('home4.social');
            $view4['popular'] = view('home4.popular');
            $view4['follow-us2'] = view('home4.follow-us');
            $view4['subscribe'] = view('home4.subscribe');
            $view4['addvert'] = view('home4.addvert');
            return view('welcome4',$view4);   
        }elseif ($theme->status == 5) {
            $view5['details'] = view('home5.details',compact('new','latestNews','comments'));
            $view5['follow-us'] = view('home5.social');
            $view5['popular'] = view('home5.popular');
            $view5['follow-us2'] = view('home5.follow-us');
            $view5['subscribe'] = view('home5.subscribe');
            $view5['addvert'] = view('home5.addvert');
            //$view5['title'] = $title;
            View::share('title', $title);
            View::share('meta_keyword', $new->meta_keyword);
            View::share('meta_description', $new->meta_description);

            return view('welcome5',$view5);   
        }
    }
    public function comment(Request $request, $post_id) {
        request()->validate([
            'comment' => 'required|string|max:2500',
        ], [
            'comment.required' => 'The Comment field is required.',
        ]);
        $comment = Comment::create([
            'user_id' => Sentinel::getUser()->id,
            'news_id' => $post_id,
            'comment' => $request->input('comment'),
        ]);

        if (!empty($comment->id)) {
            return redirect()->back()->with('message', 'Comment add successfully.');
        } else {
            return redirect()->back()->with('exception', 'Operation failed !');
        }
    }
    public function authorDetail($id,$name){
        $author = User::findOrFail($id);
        $news= $author->posts()->paginate(3);
        
        $view5['details'] = view('home5.author_detail',compact('news','author'));
        $view5['follow-us'] = view('home5.social');
        $view5['popular'] = view('home5.popular');
        $view5['follow-us2'] = view('home5.follow-us');
        $view5['subscribe'] = view('home5.subscribe');
        $view5['addvert'] = view('home5.addvert');
       return view('welcome5',$view5);   
    }
    public function authorAll(){
        $role = Sentinel::findRoleById(3);

        $authors = $role->users()->get();
      //  $authors = Sentinel::getUserRepository()->with('roles','activations')->get();
         /*foreach($authors as $user)
          {         
             echo "<pre>";print_r($user);
            echo "</pre>"; 
        }die;
*/

        $view5['details'] = view('home5.author_all',compact('authors'));
        $view5['follow-us'] = view('home5.social');
        $view5['popular'] = view('home5.popular');
        $view5['follow-us2'] = view('home5.follow-us');
        $view5['subscribe'] = view('home5.subscribe');
        $view5['addvert'] = view('home5.addvert');
       return view('welcome5',$view5);   
    }
    public function authorRegister(){
        $view5['details'] = view('home5.author_register');
        $view5['follow-us'] = view('home5.social');
        $view5['popular'] = view('home5.popular');
        $view5['follow-us2'] = view('home5.follow-us');
        $view5['subscribe'] = view('home5.subscribe');
        $view5['addvert'] = view('home5.addvert');
       return view('welcome5',$view5);   
    }
    public function addAuthor(Request $request)
    {   
        $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'password' => 'required|min:6|confirmed',
            ]);

        $data = array();
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['password'] = $request->password;
        $role = 3;
        $user = Sentinel::registerAndActivate($data);
        $user->roles()->attach($role); 
        alert()->success('Good Job', 'Thank you for registration !!');
          return redirect('login');
        //return redirect()->back();
    }
    public function userRegister(){

        $view5['details'] = view('home5.user_register');
        $view5['follow-us'] = view('home5.social');
        $view5['popular'] = view('home5.popular');
        $view5['follow-us2'] = view('home5.follow-us');
        $view5['subscribe'] = view('home5.subscribe');
        $view5['addvert'] = view('home5.addvert');
       return view('welcome5',$view5);   
    }
    public function addUser(Request $request)
    {   
        $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
               // 'phone' => 'required',
                'email' => 'required',
                'password' => 'required|min:6|confirmed',
            ]);
       /* echo "<pre>";
        print_r($request);
        echo "</pre>";die;*/
        $data = array();
        $data['first_name'] = $request->first_name;
        $data['last_name'] = $request->last_name;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['password'] = $request->password;
        $role = 4;
        $user = Sentinel::registerAndActivate($data);
        $user->roles()->attach($role); 
        alert()->success('Good Job', 'Thank you for registration !!');
        return redirect('login');

        //return  Redirect::route('login');//redirect()->back();
    }
    public function videos($id, $title)
    {
        $theme = Theme::first();
        $Video = Video::where('id',$id)->get();

        if($theme->status == 1) {
            $view['video'] = view('home.videodetails',compact('Video'));
            $view['addvert'] = view('home.addvert');
            $view['follow-us'] = view('home.social');
            $view['popular'] = view('home.popular'); 
            // $view['subscribe'] = view('home.subscribe');
            return view('welcome',$view);
        }elseif($theme->status == 2) {
            $view2['videodetails'] = view('home2.videodetails',compact('Video'));
            $view2['addvert'] = view('home2.addvert');
            $view2['follow-us'] = view('home2.social');
            $view2['subscribe'] = view('home2.subscribe');
            return view('welcome2',$view2);
        }elseif ($theme->status == 3) {
            $view3['videodetails'] = view('home3.videodetails',compact('Video'));
            $view3['addvert'] = view('home3.addvert');
            $view3['follow-us'] = view('home3.social');
            $view3['subscribe'] = view('home3.subscribe');
            return view('welcome3',$view3);   
        }elseif ($theme->status == 4) {
            $view4['videodetails'] = view('home4.videodetails',compact('Video'));
            $view4['addvert'] = view('home4.addvert');
            $view4['follow-us'] = view('home4.social');
            $view4['subscribe'] = view('home4.subscribe');
            return view('welcome4',$view4);   
        }
    }

    public function aboutUs()
    {
        $theme = Theme::first();
        if($theme->status == 1) {
            $view['about'] = view('home.about');
            $view['addvert'] = view('home.addvert');
            $view['follow-us'] = view('home.social');
            return view('welcome',$view);
        }elseif ($theme->status == 2) {
            $view2['about'] = view('home2.about');
            $view2['addvert'] = view('home2.addvert');
            $view2['follow-us'] = view('home2.social');
            return view('welcome2',$view2);
        }elseif ($theme->status == 3) {
            $view3['about'] = view('home3.about');
            $view3['addvert'] = view('home3.addvert');
            $view3['follow-us'] = view('home3.social');
            return view('welcome3',$view3);   
        }elseif ($theme->status == 4) {
            $view4['about'] = view('home4.about');
            $view4['addvert'] = view('home4.addvert');
            $view4['follow-us'] = view('home4.social');
            return view('welcome4',$view4);   
        }
    }
    public function contactUs(){
        $theme = Theme::first();

        if($theme->status == 1) {
            $view['contact'] = view('home.contact');
            $view['addvert'] = view('home.addvert');
            $view['follow-us'] = view('home.social');
            $view['popular'] = view('home.popular'); 
            // $view['subscribe'] = view('home.subscribe');
            return view('welcome', $view);
        }elseif ($theme->status == 2) {
            $view2['contact'] = view('home2.contact');
            $view2['addvert'] = view('home2.addvert');
            $view2['follow-us'] = view('home2.social');
            // $view2['popular'] = view('home2.popular'); 
            $view2['subscribe'] = view('home2.subscribe');
            return view('welcome2', $view2);
        }elseif ($theme->status == 3) {
            $view3['about'] = view('home3.contact');
            $view3['addvert'] = view('home3.addvert');
            $view3['follow-us'] = view('home3.social');
            $view3['subscribe'] = view('home3.subscribe');
            return view('welcome3',$view3);   
        }elseif ($theme->status == 4) {
            $view4['about'] = view('home4.contact');
            $view4['addvert'] = view('home4.addvert');
            $view4['follow-us'] = view('home4.social');
            $view4['subscribe'] = view('home4.subscribe');
            return view('welcome4',$view4);   
        }elseif ($theme->status == 5) {
            $view5['about'] = view('home5.contact');
            $view5['addvert'] = view('home5.addvert');
            $view5['follow-us'] = view('home5.social');
            $view5['subscribe'] = view('home5.subscribe');
            return view('welcome5',$view5);   
        }
    }
    public function submitContact(Request $request)
    {
        $gsetting = Gsetting::first();
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ]);


        $to = $gsetting->email;
        $subject = $request->subject;
        $msg = $request->message;
        $name = $request->name;
        $email = $request->email;

        $headers = "From: $name <$email> \r\n";
        $headers .= "Reply-To: $name <$email> \r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "X-Mailer: PHP/".phpversion();
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $message = "
                    <html>
                    <head>
                    <title>Contact Message</title>
                    </head>
                    <body>
                    <p>$msg</p>
                    </body>
                    </html>
                    ";
        if (mail($to, $subject, $message, $headers)) {
            alert()->success('Success', 'Message send.');
            return redirect()->back();
        } else {
            alert()->error('Failed', 'Message not send');
        }
    }
    public function privacy()
    {
        $theme = Theme::first();
        if($theme->status == 1) {
            $view['privacy'] = view('home.privacy');
            $view['addvert'] = view('home.addvert');
            $view['follow-us'] = view('home.social');
            return view('welcome',$view);
        }elseif ($theme->status == 2) {
            $view2['privacy'] = view('home2.privacy');
            $view2['addvert'] = view('home2.addvert');
            $view2['follow-us'] = view('home2.social');
            return view('welcome2',$view2);
        }elseif ($theme->status == 3) {
            $view3['privacy'] = view('home3.privacy');
            $view3['addvert'] = view('home3.addvert');
            $view3['follow-us'] = view('home3.social');
            return view('welcome3',$view3);   
        }elseif ($theme->status == 4) {
            $view4['privacy'] = view('home4.privacy');
            $view4['addvert'] = view('home4.addvert');
            $view4['follow-us'] = view('home4.social');
            return view('welcome4',$view4);   
        }   
    }

    public function sitemap()
    {
        $theme = Theme::first();
        if($theme->status == 1) {
            $view['sitemap'] = view('home.sitemap');
            $view['addvert'] = view('home.addvert');
            $view['follow-us'] = view('home.social');
            return view('welcome',$view);
        }elseif ($theme->status == 2) {
            $view2['sitemap'] = view('home2.sitemap');
            $view2['addvert'] = view('home2.addvert');
            $view2['follow-us'] = view('home2.social');
            return view('welcome2',$view2);
        }elseif ($theme->status == 3) {
            $view3['sitemap'] = view('home3.sitemap');
            $view3['addvert'] = view('home3.addvert');
            $view3['follow-us'] = view('home3.social');
            return view('welcome3',$view3);   
        }elseif ($theme->status == 4) {
            $view4['sitemap'] = view('home4.sitemap');
            $view4['addvert'] = view('home4.addvert');
            $view4['follow-us'] = view('home4.social');
            return view('welcome4',$view4);   
        }
    }
    public function clickadd($id){
        $hitNews = Advertisement::findOrFail($id);
        $data = array();
        $data['hit'] = $hitNews->hit+1;
        Advertisement::where('id',$id)->update($data);

        $go = $hitNews->link;
        return redirect($go);
    }

    public function ajaxData(Request $request)
    {
        $data = News::select("title as name")
                        ->where("title","LIKE","%{$request->input('query')}%")
                        ->get();
        return response()->json($data);
    }

    public function search()
    {
        $theme = Theme::first();
         $searchterm = Input::get('searchinput');
          if ($searchterm) {

            $news = News::where('status',1)->where('title', 'LIKE', '%' . $searchterm . '%')
                    ->orWhere('source', 'LIKE', '%' . $searchterm . '%')
                    ->paginate(10);
            if($theme->status == 1) {
                $data['search'] = view('home.search',compact('news'));
                $data['addvert'] = view('home.addvert');
                $data['follow-us'] = view('home.social');
                $data['popular'] = view('home.popular'); 
                return view('welcome', $data);
            }
            elseif($theme->status == 2) {
                $data2['search'] = view('home2.search',compact('news'));
                $data2['addvert'] = view('home2.addvert');
                $data2['follow-us'] = view('home2.social');
                $data2['popular'] = view('home2.popular'); 
                return view('welcome2', $data2);
            }
            elseif($theme->status == 3) {
                $data3['search'] = view('home3.search',compact('news'));
                $data2['addvert'] = view('home3.addvert');
                $data3['follow-us'] = view('home3.social');
                $data3['popular'] = view('home3.popular'); 
                return view('welcome3', $data3);
            }
            elseif($theme->status == 4) {
                $data4['search'] = view('home4.search',compact('news'));
                $data4['addvert'] = view('home4.addvert');
                $data4['follow-us'] = view('home4.social');
                $data4['popular'] = view('home4.popular'); 
                return view('welcome4', $data4);
            }
            elseif($theme->status == 5) {//var_dump($news);die;
                $data5['search'] = view('home5.search',compact('news'));
                $data5['addvert'] = view('home5.addvert');
                $data5['follow-us'] = view('home5.social');
                $data5['popular'] = view('home5.popular'); 
                return view('welcome5', $data5);
            }
        }
    }

}
