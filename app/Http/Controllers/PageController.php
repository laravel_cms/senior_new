<?php

namespace App\Http\Controllers;

use App\Page;
use App\Category;
use Illuminate\Http\Request;
use DB;
use Session;
use Carbon\Carbon;
use File;
use Intervention\Image\Facades\Image;
use Input;
use Sentinel;
class PageController extends Controller
{
  public function __construct()
    {
        $this->middleware('role:admin-manager');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $menu = view('dashboard.menubar');
        $allPages = Page::orderBy('created_at', 'desc')->get();
        $content = view('dashboard.page.pages',compact('allPages','category'));
        return view('dashboard', compact('menu','content'));
    }
    public function addpage()
    {
        $menu = view('dashboard.menubar');
        $content = view('dashboard.page.add-page');
        return view('dashboard', compact('menu','content'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
                'title' => 'required',
                'details' => 'required',
            ]);
        $data = array();
        $data['title'] = $request->title;
        $data['slug'] = str_slug($request->title);
        $data['details'] = $request->details;
        $data['user_id'] = Sentinel::getUser()->id;
        $data['created_at'] = Carbon::now(); 
        $data['updated_at'] = Carbon::now(); 

        Page::create($data);
        alert()->success('Good Job', 'Successfully added a post !!');
            return redirect('/add-page');
    }


    public function editpage(Request $request, $id)
    {
        $page = Page::findOrFail($id);
        $menu = view('dashboard.menubar');
        $content = view('dashboard.page.edit-page',compact('page'));
        return view('dashboard', compact('menu','content'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
       
        $data['title'] = $request->title;
        $data['slug'] = str_slug($request->title);
        $data['details'] = $request->details;
        $data['updated_at'] = Carbon::now(); 
        $data['user_id'] = Sentinel::getUser()->id;
          
       // Upload the new picture
        DB::table('pages')->where('id', $id)->update($data);
        alert()->success('Good Job', 'Successfully Updated !!');
        return redirect('/pages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pages  $pages
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $pages = Page::find($id);

        $path = public_path('images'). '/' .$pages->image;
         if($pages->image == null)
            {
                $pages->delete();
            }else{
                unlink($path);
            }

        $pages->delete();
        alert()->success('Good Job', 'Successfully Deleted !!');
        return back();
    }


    public function publish($id)
    {
        Page::where('id', $id)->update(['status' => 1]); 
        alert()->success('Good Job', 'Successfully Published !!');
        return back();
    }
    public function unpublish($id)
    {
        Page::where('id', $id)->update(['status' => 0]); 
        alert()->success('Good Job', 'Successfully Unpublished !!');
        return back();
    }
}
