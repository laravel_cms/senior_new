<?php

namespace App;

use Illuminate\Notifications\Notifiable;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Authenticatable;
use Cartalyst\Sentinel\Users\EloquentUser;
class User extends EloquentUser
{
    use Notifiable, Authenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // public function roles(){
    //         return $this->belongsToMany('App\Role');
    // }
    public function posts() {
        return $this->hasMany('App\News')->where('status',1)->latest();
    }
    public function comments() {
        return $this->hasMany('App\Comment')->latest();
    }
    public function get_gravatar(){
         /*$s = 80; $d = 'mm';$r = 'g'; $img = false; $atts = array();
        $url = 'https://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim($this->email)));
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';
            foreach ($atts as $key => $val) {
                $url .= ' ' . $key . '="' . $val . '"';
            }

            $url .= ' />';
        }
        return $url;*/
       return  asset('public/images/'.$this->profile) ;
    }

    public function link(){
        return url('author/'.$this->id.'/'.$this->first_name.'_'.$this->last_name);
    }
    public function full_name(){
        return $this->first_name.' '.$this->last_name;
    }
}
