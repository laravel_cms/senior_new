<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {
	protected $fillable = [
		'user_id', 'news_id', 'parent_comment_id', 'comment', 'publication_status',
	];

	public function user() {
        return $this->belongsTo('App\User');
	}

	public function news() {
        return $this->belongsTo('App\News');
	}
}
