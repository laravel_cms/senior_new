(function($){
	"use strict"; 
	/**
	* Header Area start
	*/
	$("body.header-sticky header").addClass("animated");
	$(window).on('scroll',function() {    
		var scroll = $(window).scrollTop();
		if (scroll < 245) {
			$("body.header-sticky header").removeClass("is-sticky");
		}else{
			$("body.header-sticky header").addClass("is-sticky");
		}
	}); 

	$("header.header-sticky").addClass("animated");
	$(window).on('scroll',function() {    
		var scroll = $(window).scrollTop();
		if (scroll < 245) {
			$("header.header-sticky").removeClass("is-sticky");
		}else{
			$("header.header-sticky").addClass("is-sticky");
		}
	}); 


	if ( $('body').hasClass('logged-in') ) {
		var top_offset = $('.header-area').height() + 32;
	} else {
		var top_offset = $('.header-area').height() - 0;
	}

	$('.primary-nav-one-page nav').onePageNav({
	     scrollOffset: top_offset,
		 scrollSpeed: 750,
		 easing: 'swing',
		 currentClass: 'active',
	});

	$('body').scrollspy({target: ".primary-nav-wrap nav"});
	$(".primary-nav-one-page nav ul li:first-child").addClass("active"); 

	$('.primary-nav-wrap > nav > ul > li').slice(-2).addClass('last-elements');
	
    /*-- Mobile Menu --*/

    $('.primary-nav-wrap nav').meanmenu({
        meanScreenWidth: mobile_menu_data.menu_width,
        meanMenuContainer: '.mobile-menu',
        meanMenuClose: '<i class="fa fa-times"></i>',
        meanMenuOpen: '<i class="fa fa-bars"></i>',
        meanRevealPosition: 'right',
        meanMenuCloseSize: '25px',
    });


	/*
    * Header Transparent 
    */
    function headerTransparentTopbar(){
    	var headerTopbarHeight = $('.header-top-area').innerHeight(),
    		trigger = $('.main-header.header-transparent'),
    		bodyTrigger = $('body.logged-in .main-header.header-transparent');
    	if( trigger.parents().find('.header-top-area') ){
    		trigger.css('top', headerTopbarHeight + 'px');
    	}
    	if( bodyTrigger.parents().find('.header-top-area') ){
    		bodyTrigger.css('top', (headerTopbarHeight + 32) + 'px' );
    	}
    }
    headerTransparentTopbar();

    /**
    * ScrollUp
    */
	function backToTop(){

		var didScroll = false,
			scrollTrigger = $('#back-to-top');
		
		scrollTrigger.on('click',function(e) {
			$('body,html').animate({ scrollTop: "0" });
			e.preventDefault();
		});
		
		$(window).scroll(function() {
			didScroll = true;
		});

		setInterval(function() {
			if( didScroll ) {
				didScroll = false;
		
				if( $(window).scrollTop() > 250 ) {
					scrollTrigger.css('right',20);
				} else {
					scrollTrigger.css('right','-50px');
				}
			}
		}, 250);
	}
	backToTop();

	/**
	* Wow init
	*/
	new WOW().init();


	/**
	* Magnific Popup video popup 
	*/
	$('a.video-popup').magnificPopup({
		type: 'iframe',
		closeOnContentClick: false,
		closeBtnInside: false,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		image: {
			verticalFit: true,
			titleSrc: function(item) {
				return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
			}
		},
		gallery: {
			enabled: false
		},
		zoom: {
			enabled: true,
			duration: 300, // don't foget to change the duration also in CSS
			opener: function(element) {
				return element.find('img');
			}
		}
		
	});

	/**
	* Blog Gallery Post
	*/
	$('.blog-gallery').owlCarousel({
	    loop:true,
	    nav:true,
	    navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
	    responsive:{
	        0:{
	            items:1
	        },
	        768:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	})

	/**
	* Enable Footer Fixed effect
	*/
	function fixedFooter(){
		var fooCheck = $('footer').hasClass('fixed-footer-enable');
		if(fooCheck){
			$('.site-wrapper').addClass('fixed-footer-active'); 
		}
		var FooterHeight = $('footer.fixed-footer-enable').height(),
			winWidth = $(window).width();
		if( winWidth > 991 ){
			$('.fixed-footer-active').css({'margin-bottom': FooterHeight});
			$('.fixed-footer-active .site-content').css({'background': '#ffffff'});
		} else{
			$('footer').removeClass('fixed-footer-enable');
		}
	}
	fixedFooter();

	/**
	* Page Preloading Effects
	*/
	$(window).on('load', function(){
		$(".loading-init").fadeOut(500);
	});


	/**
	* Blog Masonry
	*/
	$('.blog-masonry').imagesLoaded( function() {
		// init Isotope
		var $grid = $('.blog-masonry').isotope({
		  itemSelector: '.grid-item',
		  percentPosition: true,
		  masonry: {
			// use outer width of grid-sizer for columnWidth
			columnWidth: '.grid-item',
		  }
		});
	});
	
	
	/*-- 
    Header Search
	--------------------------------------------*/
	var searchToggle = $('.header-search-toggle');
	var searchForm = $('.header-search-form');

	searchForm.hide();
	/*-- Search Toggle --*/
	searchToggle.on('click', function(){
	    if( searchToggle.hasClass('open') ) {
	        searchForm.animate({
	            width: "toggle",
	        });
	        $(this).removeClass('open').find('i').removeClass('fa-close').addClass('fa-search');
	    }else{
	        searchForm.animate({
	            width: "toggle",
	        });
	        $(this).addClass('open').find('i').removeClass('fa-search').addClass('fa-close');
	    }
	});

	/*--
    Slick Slider
	-----------------------------------*/

	/*-- 
    Breaking News Ticker
	--------------------------------------------*/
	$('.breaking-news-ticker').newsTicker({
	    row_height: 40,
	    max_rows: 1,
	    speed: 600,
	    duration: 5000,
	    prevButton:  $('.news-ticker-prev'),
	    nextButton:  $('.news-ticker-next'),
	});

	/*-- Post Carousel --*/
	$('.post-carousel-1').slick({
	    autoplay: true,
	    autoplaySpeed: 5000,
	    pauseOnFocus: false,
	    pauseOnHover: false,
	    infinite: true,
	    slidesToShow: 1,
	    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
	    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
	    responsive: [
	        {
	          breakpoint: 350,
	          settings: {
	            arrows: false,
	          }
	        }
	    ]
	});
	
	/*-- Popular Post Slider --*/
	$('.popular-post-slider').slick({
	    arrows: false,
	    autoplay: true,
	    autoplaySpeed: 5000,
	    pauseOnFocus: false,
	    pauseOnHover: false,
	    infinite: true,
	    slidesToShow: 3,
	    responsive: [
	        {
	          breakpoint: 1199,
	          settings: {
	            slidesToShow: 2,
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 2,
	          }
	        },
	        {
	          breakpoint: 767,
	          settings: {
	            slidesToShow: 1,
	          }
	        }
	    ]
	});

	/*-- Two Column Post Carousel --*/
	$('.two-column-post-carousel').slick({
	    arrows: true,
	    autoplay: true,
	    autoplaySpeed: 5000,
	    pauseOnFocus: false,
	    pauseOnHover: false,
	    infinite: true,
	    slidesToShow: mobile_menu_data.related_post_number,
	    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
	    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
	    responsive: [
	        {
	          breakpoint: 767,
	          settings: {
	            slidesToShow: 1,
	          }
	        }
	    ]
	});

	/*-- Four Column Post Carousel --*/
	$('.four-column-post-carousel').slick({
	    arrows: false,
	    autoplay: true,
	    autoplaySpeed: 5000,
	    pauseOnFocus: false,
	    pauseOnHover: false,
	    infinite: true,
	    slidesToShow: 4,
	    responsive: [
	        {
	          breakpoint: 1199,
	          settings: {
	            slidesToShow: 3,
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 2,
	          }
	        },
	        {
	          breakpoint: 767,
	          settings: {
	            slidesToShow: 1,
	          }
	        }
	    ]
	});

	/*-- Five Row Post Carousel --*/
	$('.five-row-post-carousel').slick({
	    autoplay: false,
	    autoplaySpeed: 5000,
	    pauseOnFocus: false,
	    pauseOnHover: false,
	    infinite: true,
	    slidesToShow: 1,
	    rows: 5,
	    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
	    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
	    responsive: [
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 2,
	            rows: 4,
	          }
	        },
	        {
	          breakpoint: 767,
	          settings: {
	            slidesToShow: 1,
	          }
	        }
	    ]
	});
	    
	
	/*-- Three Row Post Carousel --*/
	$('.three-row-post-carousel').slick({
	    autoplay: false,
	    autoplaySpeed: 5000,
	    pauseOnFocus: false,
	    pauseOnHover: false,
	    infinite: true,
	    slidesToShow: 1,
	    rows: 3,
	    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
	    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
	});

	/*-- Two Row Post Carousel --*/
	$('.two-row-post-carousel').slick({
	    autoplay: false,
	    autoplaySpeed: 5000,
	    pauseOnFocus: false,
	    pauseOnHover: false,
	    infinite: true,
	    slidesToShow: 1,
	    rows: 2,
	    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
	    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
	});

	/*-- Sidebar Post Carousel --*/
	$('.sidebar-post-carousel').slick({
	    autoplay: true,
	    autoplaySpeed: 5000,
	    pauseOnFocus: false,
	    pauseOnHover: false,
	    infinite: true,
	    slidesToShow: 1,
	    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
	    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
	});


	/*-- Full Width Instagram Carousel --*/
	$('.fullwidth-instagram-carousel').slick({
	    arrows: false,
	    autoplay: true,
	    autoplaySpeed: 5000,
	    pauseOnFocus: false,
	    pauseOnHover: false,
	    infinite: true,
	    slidesToShow: 6,
	    responsive: [
	        {
	          breakpoint: 1199,
	          settings: {
	            slidesToShow: 4,
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 3,
	          }
	        },
	        {
	          breakpoint: 767,
	          settings: {
	            slidesToShow: 2,
	          }
	        },
	        {
	          breakpoint: 350,
	          settings: {
	            slidesToShow: 1,
	          }
	        }
	    ]
	});

	/*--
    Magnific Video Popup
	--------------------------------*/
	var imagePopup = $('.image-popup');
	imagePopup.magnificPopup({
	    type: 'image',
	    tLoading: 'Loading image #%curr%...',
	    gallery: {
	        enabled: true,
	    },
	});
	var videoPopup = $('.video-popup');
	videoPopup.magnificPopup({
	    type: 'iframe',
	    mainClass: 'mfp-fade',
	    removalDelay: 160,
	    preloader: false,
	    zoom: {
	        enabled: true,
	    }
	});

	/*-- Three Column Post Carousel --*/
	$('.three-column-post-carousel').slick({
	    arrows: true,
	    autoplay: true,
	    autoplaySpeed: 5000,
	    pauseOnFocus: false,
	    pauseOnHover: false,
	    infinite: true,
	    slidesToShow: 3,
	    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
	    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
	    responsive: [
	        {
	          breakpoint: 1199,
	          settings: {
	            slidesToShow: 3,
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 2,
	          }
	        },
	        {
	          breakpoint: 767,
	          settings: {
	            slidesToShow: 1,
	          }
	        }
	    ]
	});

	function slickRowActive(){
		
		$('.slick-row-active').each(function(){
			var $this = $(this);
			var autoplay = $this.data('slick-autoplay');
			var arrows = $this.data('slick-arrows');
			var items = $this.data('slick-items');
			var rows = $this.data('slick-rows');
			var autoplaySpeed = $this.data('slick-autoplaySpeed');
			var loop = $this.data('slick-loop');
			var desktop = $this.data('slick-desktop');
			var tablate = $this.data('slick-tablate');

			/*-- Four Row Post Carousel --*/
			$this.slick({
				arrows: arrows,
				autoplay: autoplay,
				autoplaySpeed: autoplaySpeed,
				pauseOnFocus: false,
				pauseOnHover: false,
				infinite: loop,
				slidesToShow: items,
				rows: rows,
				prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
				nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
				responsive: [
					{
					  breakpoint: 1199,
					  settings: {
						rows: desktop,
					  }
					},
					{
					  breakpoint: 991,
					  settings: {
						rows: tablate,
					  }
					},
					{
					  breakpoint: 767,
					  settings: {
						rows: 1,
					  }
					}
				]
			});
		});
	};
	slickRowActive();

	/*-- Weather active --*/
	function loadWeather(location, woeid) {
	  $.simpleWeather({
	    location: location,
	    woeid: woeid,
	    unit: 'c',
	    success: function(weather) {
	      var html = '<i class="we-icon icon-'+weather.code+'"></i>'+'<span class="weather-degrees">'+weather.temp+'&deg; <span class="unit">'+weather.units.temp+'</span> </span><span class="weather-location">-'+weather.city+'</span>';
	      
	      $(".weather-wrap").html(html);
	    },
	    error: function(error) {
	      $(".weather-wrap").html('<p>'+error+'</p>');
	    }
	  });
	}
	    
	navigator.geolocation.getCurrentPosition(function(position) {
	    loadWeather(position.coords.latitude+','+position.coords.longitude); //load weather using your lat/lng coordinates
	});
	loadWeather('Seattle',''); //@params location, woeid
	

	
})(jQuery);